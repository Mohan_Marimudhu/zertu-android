package com.medialima.zertu.network;

import android.content.Context;
import android.text.TextUtils;

import com.medialima.zertu.model.AuthenticationResponseModel;
import com.medialima.zertu.model.LoginRequestModel;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.UserUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ResponseCallback<T> implements Callback<T> {

    private RestClient mRestClient = null;
    private Context mContext;

    public ResponseCallback(Context context) {
        mContext = context;
        mRestClient = new RestClient(mContext);
    }

    public abstract void onResponse(T response);

    public abstract void onFailure();


    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.errorBody() != null && !response.isSuccessful()) {
            if (!TextUtils.isEmpty(UserUtils.getAccessToken(mContext)))
                getAccessToken(call);
            else
                onFailure();
        } else {
            onResponse(response.body());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (!TextUtils.isEmpty(UserUtils.getAccessToken(mContext)))
            getAccessToken(call);
        else
            onFailure();
    }

    private void getAccessToken(final Call<T> mainCall) {
        ApiService apiInterface = mRestClient.getService();
        LoginRequestModel loginUserModel = new LoginRequestModel();
        loginUserModel.setPassword(UserUtils.getUserPassword(mContext));
        loginUserModel.setUsername(UserUtils.getEmailId(mContext));
        loginUserModel.setGrant_type(Constants.REFRESH_TOKEN);
        loginUserModel.setClient_secret(Constants.CLIENT_SECRETE);
        loginUserModel.setClient_id(Constants.CLIENT_ID);
        Call<AuthenticationResponseModel> call = apiInterface.login(loginUserModel.getPassword(),
                loginUserModel.getUsername(),
                loginUserModel.getGrant_type(),
                loginUserModel.getScope(),
                loginUserModel.getClient_secret(),
                loginUserModel.getClient_id(),UserUtils.getRefreshToken(mContext));
        call.enqueue(new Callback<AuthenticationResponseModel>() {
            @Override
            public void onResponse(Call<AuthenticationResponseModel> call, final Response<AuthenticationResponseModel> response) {
                if (response.body() != null) {
                    UserUtils.setAccessToken(mContext, response.body().getAccessToken());
                    mainCall.clone().enqueue(new Callback<T>() {
                        @Override
                        public void onResponse(Call<T> call, Response<T> response) {
                            if (response.errorBody() != null) {
                                ResponseCallback.this.onFailure();
                            } else {
                                ResponseCallback.this.onResponse(response.body());
                            }
                        }

                        @Override
                        public void onFailure(Call call, Throwable t) {
                            ResponseCallback.this.onFailure();
                        }
                    });
                } else {
                    ResponseCallback.this.onFailure();
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponseModel> call, Throwable t) {
                ResponseCallback.this.onFailure();
            }
        });
    }
}
