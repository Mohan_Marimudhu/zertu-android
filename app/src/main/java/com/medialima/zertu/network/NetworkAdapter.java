package com.medialima.zertu.network;

import android.content.Context;

import com.medialima.zertu.model.AllCategoryResponseModel;
import com.medialima.zertu.model.AuthenticationResponseModel;
import com.medialima.zertu.model.CardModel;
import com.medialima.zertu.model.CategoryDetailResponseModel;
import com.medialima.zertu.model.ConecktaPaymentResponseModel;
import com.medialima.zertu.model.ContactResponseModel;
import com.medialima.zertu.model.CourseDetailResponseModel;
import com.medialima.zertu.model.CreatePaymentRequestModel;
import com.medialima.zertu.model.CurrentVersionResponseModel;
import com.medialima.zertu.model.FbLoginRequestModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.LoginRequestModel;
import com.medialima.zertu.model.OXXOPaymentResponseModel;
import com.medialima.zertu.model.OxxoRequestModel;
import com.medialima.zertu.model.PasswordRecoveryRequestModel;
import com.medialima.zertu.model.PayPalTokenResponseModel;
import com.medialima.zertu.model.PaymentRequestModel;
import com.medialima.zertu.model.PaypalPaymentRequestModel;
import com.medialima.zertu.model.PaypalResponseModel;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.model.ProfileResponseModel;
import com.medialima.zertu.model.RegisterRequestModel;
import com.medialima.zertu.model.UpdateProfileRequestModel;
import com.medialima.zertu.model.UserTicketsModel;
import com.medialima.zertu.utils.UserUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Mohan M on 19/02/2017.
 */

public class NetworkAdapter {

    private static NetworkAdapter INSTANCE = null;
    private Context mContext;
    private ApiService mApiService;

    public static NetworkAdapter getInstance(Context context) {
        if (null == INSTANCE) {
            INSTANCE = new NetworkAdapter(context);
        }
        return INSTANCE;
    }

    private NetworkAdapter(Context context) {
        mContext = context;
        mApiService = new RestClient(context).getService();
    }

    public void getAllCategory(Callback<GenericResponse<ArrayList<AllCategoryResponseModel>>> callback){
        Call<GenericResponse<ArrayList<AllCategoryResponseModel>>> call=mApiService.getAllCategories();
        call.enqueue(callback);
    }
    public void findCategoryById(String id,Callback<GenericResponse<CategoryDetailResponseModel>> callback){
        Call<GenericResponse<CategoryDetailResponseModel>> call=mApiService.findCategoryById(id);
        call.enqueue(callback);
    }

    public void getCourseById(String id,Callback<GenericResponse<CourseDetailResponseModel>> callback){
        Call<GenericResponse<CourseDetailResponseModel>> call=mApiService.getCourseById(id);
        call.enqueue(callback);
    }

    public void register(RegisterRequestModel registerRequestModel, Callback<GenericResponse<String>> callback){
        Call<GenericResponse<String>> call=mApiService.register(registerRequestModel);
        call.enqueue(callback);
    }


    public void login(LoginRequestModel loginUserModel, Callback<AuthenticationResponseModel> callback) {
        Call<AuthenticationResponseModel> call = mApiService.login(loginUserModel.getPassword(),
                loginUserModel.getUsername(),
                loginUserModel.getGrant_type(),
                loginUserModel.getScope(),
                loginUserModel.getClient_secret(),
                loginUserModel.getClient_id(),
                UserUtils.getRefreshToken(mContext));
        call.enqueue(callback);

    }

    public void fbLogin(FbLoginRequestModel fbLoginRequestModel, Callback<GenericResponse<AuthenticationResponseModel>> callback) {
        Call<GenericResponse<AuthenticationResponseModel>> call = mApiService.fbLogin(fbLoginRequestModel);
        call.enqueue(callback);

    }

    public void getPersonalCourseList(Callback<GenericResponse<ArrayList<PersonalCourseListModel>>> callback){
        Call<GenericResponse<ArrayList<PersonalCourseListModel>>> call=mApiService.getPersonalCourseList();
        call.enqueue(callback);
    }

    public void getCardList(Callback<GenericResponse<ArrayList<CardModel>>> callback){
        Call<GenericResponse<ArrayList<CardModel>>> call=mApiService.getCardList();
        call.enqueue(callback);
    }
    public void getPersonalCourseDetail(String id,Callback<GenericResponse<PersonalCourseListModel>> callback){
        Call<GenericResponse<PersonalCourseListModel>> call=mApiService.getPersonalCourseDetail(id);
        call.enqueue(callback);
    }

    public void paypalTokenReques(Callback<PayPalTokenResponseModel> callback){
        Call<PayPalTokenResponseModel> call=mApiService.paypalTokenRequest();
        call.enqueue(callback);
    }
    public void createPaymentRequestModel(CreatePaymentRequestModel createPaymentRequestModel,Callback<GenericResponse> callback){
        Call<GenericResponse> call=mApiService.createPaymentRequestModel(createPaymentRequestModel);
        call.enqueue(callback);
    }

    public void changeDefaultPayment(CreatePaymentRequestModel createPaymentRequestModel,Callback<GenericResponse> callback){
        Call<GenericResponse> call=mApiService.changeDefaultPayment(createPaymentRequestModel);
        call.enqueue(callback);
    }

    public void payWithConeckta(PaymentRequestModel paymentRequestModel, Callback<GenericResponse<ConecktaPaymentResponseModel>> callback){
        Call<GenericResponse<ConecktaPaymentResponseModel>> call=mApiService.payWithConeckta(paymentRequestModel);
        call.enqueue(callback);
    }

    public void payWithOXXO(PaymentRequestModel paymentRequestModel, Callback<GenericResponse<OXXOPaymentResponseModel>> callback){
        Call<GenericResponse<OXXOPaymentResponseModel>> call=mApiService.payWithOXXO(paymentRequestModel);
        call.enqueue(callback);
    }

    public void payWithPaypal(PaypalPaymentRequestModel paymentRequestModel, Callback<GenericResponse<PaypalResponseModel>> callback){
        Call<GenericResponse<PaypalResponseModel>> call=mApiService.payWithPaypal(paymentRequestModel);
        call.enqueue(callback);
    }

    public void requestPasswordRecovery(PasswordRecoveryRequestModel passwordRecoveryRequestModel, Callback<GenericResponse<String>> callback){
        Call<GenericResponse<String>> call=mApiService.requestPasswordRecovery(passwordRecoveryRequestModel);
        call.enqueue(callback);
    }


    public void getContactInfo( Callback<GenericResponse<ContactResponseModel>> callback){
        Call<GenericResponse<ContactResponseModel>> call=mApiService.getContactInfo();
        call.enqueue(callback);
    }

    public void updateProfile(UpdateProfileRequestModel updateProfileRequestModel, Callback<GenericResponse<String>> callback){
        Call<GenericResponse<String>> call=mApiService.updateProfile(updateProfileRequestModel);
        call.enqueue(callback);
    }
    public void getProfile( Callback<GenericResponse<ProfileResponseModel>> callback){
        Call<GenericResponse<ProfileResponseModel>> call=mApiService.getProfile();
        call.enqueue(callback);
    }

    public void getCurrentVersion( Callback<GenericResponse<CurrentVersionResponseModel>> callback){
        Call<GenericResponse<CurrentVersionResponseModel>> call=mApiService.getCurrentVersion();
        call.enqueue(callback);
    }

    public void getTickets( Callback<GenericResponse<ArrayList<UserTicketsModel>>> callback){
        Call<GenericResponse<ArrayList<UserTicketsModel>>> call=mApiService.getTickets();
        call.enqueue(callback);
    }

    public void getOxxoTickets(Callback<GenericResponse<ArrayList<OxxoRequestModel>>> callback){
        Call<GenericResponse<ArrayList<OxxoRequestModel>>> call=mApiService.getOxxoTickets();
        call.enqueue(callback);
    }
}
