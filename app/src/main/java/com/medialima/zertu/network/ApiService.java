package com.medialima.zertu.network;



import com.medialima.zertu.model.AllCategoryResponseModel;
import com.medialima.zertu.model.AuthenticationResponseModel;
import com.medialima.zertu.model.CardModel;
import com.medialima.zertu.model.CategoryDetailResponseModel;
import com.medialima.zertu.model.ConecktaPaymentResponseModel;
import com.medialima.zertu.model.ContactResponseModel;
import com.medialima.zertu.model.CourseDetailResponseModel;
import com.medialima.zertu.model.CreatePaymentRequestModel;
import com.medialima.zertu.model.CurrentVersionResponseModel;
import com.medialima.zertu.model.FbLoginRequestModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.OXXOPaymentResponseModel;
import com.medialima.zertu.model.OxxoRequestModel;
import com.medialima.zertu.model.PasswordRecoveryRequestModel;
import com.medialima.zertu.model.PayPalTokenResponseModel;
import com.medialima.zertu.model.PaymentRequestModel;
import com.medialima.zertu.model.PaypalPaymentRequestModel;
import com.medialima.zertu.model.PaypalResponseModel;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.model.ProfileResponseModel;
import com.medialima.zertu.model.RegisterRequestModel;
import com.medialima.zertu.model.UpdateProfileRequestModel;
import com.medialima.zertu.model.UserTicketsModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @GET("category/find/all")
    Call<GenericResponse<ArrayList<AllCategoryResponseModel>>> getAllCategories();

    @GET("category/find/{categoryId}")
    Call<GenericResponse<CategoryDetailResponseModel>> findCategoryById(@Path("categoryId") String categoryId);

    @GET("/course/{id}")
    Call<GenericResponse<CourseDetailResponseModel>> getCourseById(@Path("id") String id);

    @POST("user/register")
    Call<GenericResponse<String>> register(@Body RegisterRequestModel registerRequestModel);

    @FormUrlEncoded
    @POST("oauth/token")
    Call<AuthenticationResponseModel> login(@Field("password") String password,
                                            @Field("username") String username,
                                            @Field("grant_type") String grant_type,
                                            @Field("scope") String scope,
                                            @Field("client_secret") String clientSecret,
                                            @Field("client_id") String clientId,
                                            @Field("refresh_token") String refreshToken);


    @POST("user/signin/facebook")
    Call<GenericResponse<AuthenticationResponseModel>> fbLogin(@Body FbLoginRequestModel fbLoginRequestModel);

    @GET("course/in-person/list")
    Call<GenericResponse<ArrayList<PersonalCourseListModel>>> getPersonalCourseList();

    @GET("course/in-person/{id}")
    Call<GenericResponse<PersonalCourseListModel>> getPersonalCourseDetail(@Path("id") String id);

    @GET("user/payment/list")
    Call<GenericResponse<ArrayList<CardModel>>> getCardList();

    @GET("/user/payment/paypal/token")
    Call<PayPalTokenResponseModel> paypalTokenRequest();

    @POST("user/payment/create")
    Call<GenericResponse> createPaymentRequestModel(@Body CreatePaymentRequestModel createPaymentRequestModel);

    @POST("user/payment/default")
    Call<GenericResponse> changeDefaultPayment(@Body CreatePaymentRequestModel createPaymentRequestModel);

    @POST("course/in-person/conekta/pay")
    Call<GenericResponse<ConecktaPaymentResponseModel>> payWithConeckta(@Body PaymentRequestModel paymentRequestModel);

    @POST("course/in-person/oxxo/pay")
    Call<GenericResponse<OXXOPaymentResponseModel>> payWithOXXO(@Body PaymentRequestModel paymentRequestModel);

    @POST("course/in-person/paypal/pay")
    Call<GenericResponse<PaypalResponseModel>> payWithPaypal(@Body PaypalPaymentRequestModel paypalPaymentRequestModel);

    @POST("user/password/recovery")
    Call<GenericResponse<String>> requestPasswordRecovery(@Body PasswordRecoveryRequestModel passwordRecoveryRequestModel);

    @GET("user/contact")
    Call<GenericResponse<ContactResponseModel>> getContactInfo();

    @POST("user/profile/update")
    Call<GenericResponse<String>> updateProfile(@Body UpdateProfileRequestModel updateProfileRequestModel);

    @GET("/user/profile")
    Call<GenericResponse<ProfileResponseModel>> getProfile();

    @GET("/user/version")
    Call<GenericResponse<CurrentVersionResponseModel>> getCurrentVersion();

    @GET("user/tickets")
    Call<GenericResponse<ArrayList<UserTicketsModel>>> getTickets();

    @GET("user/oxxo-requests")
    Call<GenericResponse<ArrayList<OxxoRequestModel>>> getOxxoTickets();

}
