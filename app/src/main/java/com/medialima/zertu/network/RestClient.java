package com.medialima.zertu.network;

import android.content.Context;

import com.medialima.zertu.BuildConfig;
import com.medialima.zertu.utils.UserUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {


    private Retrofit mRetrofit = null;

    public RestClient(final Context context) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        okHttpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                if(UserUtils.getAccessToken(context)==null){
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Accept-Encoding","identity")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }else {
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization","Bearer "+UserUtils.getAccessToken(context))
                            .header("Accept", "application/json")
                            .header("Accept-Encoding","identity")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }


            }
        });
        if (BuildConfig.DEBUG)
            okHttpClient.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient.build())
                .build();
    }

    public ApiService getService() {
        return mRetrofit.create(ApiService.class);
    }

}
