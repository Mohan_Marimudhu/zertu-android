package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Mohan M on 5/15/2018.
 */

public class CoursesModel implements Parcelable {
    String id,name,coverImageUrl,categoryId,description,primaryColor,brief;
    ArrayList<MastersModel> masters;
    ArrayList<VideosModel> videos;
    CategoryModel category;

    protected CoursesModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        coverImageUrl = in.readString();
        categoryId = in.readString();
        description = in.readString();
        primaryColor = in.readString();
        brief = in.readString();
        masters = in.createTypedArrayList(MastersModel.CREATOR);
    }

    public static final Creator<CoursesModel> CREATOR = new Creator<CoursesModel>() {
        @Override
        public CoursesModel createFromParcel(Parcel in) {
            return new CoursesModel(in);
        }

        @Override
        public CoursesModel[] newArray(int size) {
            return new CoursesModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public ArrayList<MastersModel> getMasters() {
        return masters;
    }

    public void setMasters(ArrayList<MastersModel> masters) {
        this.masters = masters;
    }

    public ArrayList<VideosModel> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<VideosModel> videos) {
        this.videos = videos;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(coverImageUrl);
        parcel.writeString(categoryId);
        parcel.writeString(description);
        parcel.writeString(primaryColor);
        parcel.writeString(brief);
        parcel.writeTypedList(masters);
    }
}
