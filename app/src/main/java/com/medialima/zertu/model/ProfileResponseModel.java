package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/28/2018.
 */

public class ProfileResponseModel {
    String uuid;
    String username;

    public String getUuid() {
        return uuid;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getFbToken() {
        return fbToken;
    }

    public String getFbRefresh() {
        return fbRefresh;
    }

    public String getEmail() {
        return email;
    }

    public String getConektaId() {
        return conektaId;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public String getSubscriptionUuid() {
        return subscriptionUuid;
    }

    public String getPassword() {
        return password;
    }

    public String getAuthorities() {
        return authorities;
    }

    public String getAccountNonLocked() {
        return accountNonLocked;
    }

    public String getCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public String getAccountNonExpired() {
        return accountNonExpired;
    }

    public String getEnabled() {
        return enabled;
    }

    String name;
    String addressLine1;
    String addressLine2;
    String fbToken;
    String fbRefresh;
    String email;
    String conektaId;
    String registeredDate;
    String profilePicUrl;
    String subscriptionUuid;
    String password;
    String authorities;
    String accountNonLocked;
    String credentialsNonExpired;
    String accountNonExpired;
    String enabled;
}
