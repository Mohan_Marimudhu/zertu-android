package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 6/12/2018.
 */

public class PersonalCourseListModel implements Parcelable {
    String id,title,description,price,maxCapacity,coverImageUrl;

    protected PersonalCourseListModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        price = in.readString();
        maxCapacity = in.readString();
        coverImageUrl = in.readString();
    }

    public static final Creator<PersonalCourseListModel> CREATOR = new Creator<PersonalCourseListModel>() {
        @Override
        public PersonalCourseListModel createFromParcel(Parcel in) {
            return new PersonalCourseListModel(in);
        }

        @Override
        public PersonalCourseListModel[] newArray(int size) {
            return new PersonalCourseListModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(String maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(price);
        parcel.writeString(maxCapacity);
        parcel.writeString(coverImageUrl);
    }
}
