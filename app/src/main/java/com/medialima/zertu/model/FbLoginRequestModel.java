package com.medialima.zertu.model;

/**
 * Created by Mohan M on 5/22/2018.
 */

public class FbLoginRequestModel {
    String fbToken;
    String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFbToken() {
        return fbToken;
    }

    public void setFbToken(String fbToken) {
        this.fbToken = fbToken;
    }

    String name;
}
