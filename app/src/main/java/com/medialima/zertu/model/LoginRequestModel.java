package com.medialima.zertu.model;

/**
 * Created by Mohan M on 5/18/2018.
 */

public class LoginRequestModel {
    String password;

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public String getScope() {
        return scope;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public String getClient_id() {
        return client_id;
    }

    String username;
    String grant_type;
    String scope;
    String client_secret;
    String client_id;
}
