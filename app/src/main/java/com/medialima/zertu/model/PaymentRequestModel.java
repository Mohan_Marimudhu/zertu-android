package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/14/2018.
 */

public class PaymentRequestModel {
    String inPersonCourseId;

    public String getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(String numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public String getInPersonCourseId() {
        return inPersonCourseId;
    }

    public void setInPersonCourseId(String inPersonCourseId) {
        this.inPersonCourseId = inPersonCourseId;
    }

    String numberOfPeople;
}
