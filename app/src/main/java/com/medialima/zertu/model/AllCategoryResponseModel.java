package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 5/14/2018.
 */

public class AllCategoryResponseModel implements Parcelable {
    String id,name,description,galleryUuid,coverImageUrl,images,primaryColor,iconUrl,contents,contentsTitle,courses,masters;

    public AllCategoryResponseModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        galleryUuid = in.readString();
        coverImageUrl = in.readString();
        images = in.readString();
        primaryColor = in.readString();
        iconUrl = in.readString();
        contents = in.readString();
        contentsTitle = in.readString();
        courses = in.readString();
        masters = in.readString();
    }

    public static final Creator<AllCategoryResponseModel> CREATOR = new Creator<AllCategoryResponseModel>() {
        @Override
        public AllCategoryResponseModel createFromParcel(Parcel in) {
            return new AllCategoryResponseModel(in);
        }

        @Override
        public AllCategoryResponseModel[] newArray(int size) {
            return new AllCategoryResponseModel[size];
        }
    };

    public AllCategoryResponseModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGalleryUuid() {
        return galleryUuid;
    }

    public void setGalleryUuid(String galleryUuid) {
        this.galleryUuid = galleryUuid;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getContentsTitle() {
        return contentsTitle;
    }

    public void setContentsTitle(String contentsTitle) {
        this.contentsTitle = contentsTitle;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }

    public String getMasters() {
        return masters;
    }

    public void setMasters(String masters) {
        this.masters = masters;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(galleryUuid);
        parcel.writeString(coverImageUrl);
        parcel.writeString(images);
        parcel.writeString(primaryColor);
        parcel.writeString(iconUrl);
        parcel.writeString(contents);
        parcel.writeString(contentsTitle);
        parcel.writeString(courses);
        parcel.writeString(masters);
    }
}
