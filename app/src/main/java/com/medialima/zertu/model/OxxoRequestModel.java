package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 7/4/2018.
 */

public class OxxoRequestModel implements Parcelable {
    String uuid,ticketId,status,datePaid,reference,title,numberOfTickets,totalAmount;

    protected OxxoRequestModel(Parcel in) {
        uuid = in.readString();
        ticketId = in.readString();
        status = in.readString();
        datePaid = in.readString();
        reference = in.readString();
        title = in.readString();
        numberOfTickets = in.readString();
        totalAmount = in.readString();
    }

    public static final Creator<OxxoRequestModel> CREATOR = new Creator<OxxoRequestModel>() {
        @Override
        public OxxoRequestModel createFromParcel(Parcel in) {
            return new OxxoRequestModel(in);
        }

        @Override
        public OxxoRequestModel[] newArray(int size) {
            return new OxxoRequestModel[size];
        }
    };

    public String getUuid() {
        return uuid;
    }

    public String getTicketId() {
        return ticketId;
    }

    public String getStatus() {
        return status;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public String getReference() {
        return reference;
    }

    public String getTitle() {
        return title;
    }

    public String getNumberOfTickets() {
        return numberOfTickets;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uuid);
        parcel.writeString(ticketId);
        parcel.writeString(status);
        parcel.writeString(datePaid);
        parcel.writeString(reference);
        parcel.writeString(title);
        parcel.writeString(numberOfTickets);
        parcel.writeString(totalAmount);
    }
}
