package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/27/2018.
 */

public class ContactResponseModel {
    String phone,email;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
