package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 6/14/2018.
 */

public class OXXOPaymentResponseModel implements Parcelable{
    String uuid;
    String ticketId;
    String status;
    String datePaid;

    protected OXXOPaymentResponseModel(Parcel in) {
        uuid = in.readString();
        ticketId = in.readString();
        status = in.readString();
        datePaid = in.readString();
        reference = in.readString();
    }

    public static final Creator<OXXOPaymentResponseModel> CREATOR = new Creator<OXXOPaymentResponseModel>() {
        @Override
        public OXXOPaymentResponseModel createFromParcel(Parcel in) {
            return new OXXOPaymentResponseModel(in);
        }

        @Override
        public OXXOPaymentResponseModel[] newArray(int size) {
            return new OXXOPaymentResponseModel[size];
        }
    };

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(String datePaid) {
        this.datePaid = datePaid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    String reference;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(uuid);
        parcel.writeString(ticketId);
        parcel.writeString(status);
        parcel.writeString(datePaid);
        parcel.writeString(reference);
    }
}
