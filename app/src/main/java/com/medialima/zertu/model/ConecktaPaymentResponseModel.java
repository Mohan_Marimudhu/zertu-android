package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 6/14/2018.
 */

public class ConecktaPaymentResponseModel implements Parcelable {
    String id;
    String inPersonCourseId;
    String name;
    String email;
    String recepit;
    String amountPaid;
    String numberOfTickets;
    String paidBy;

    protected ConecktaPaymentResponseModel(Parcel in) {
        id = in.readString();
        inPersonCourseId = in.readString();
        name = in.readString();
        email = in.readString();
        recepit = in.readString();
        amountPaid = in.readString();
        numberOfTickets = in.readString();
        paidBy = in.readString();
        status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(inPersonCourseId);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(recepit);
        dest.writeString(amountPaid);
        dest.writeString(numberOfTickets);
        dest.writeString(paidBy);
        dest.writeString(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConecktaPaymentResponseModel> CREATOR = new Creator<ConecktaPaymentResponseModel>() {
        @Override
        public ConecktaPaymentResponseModel createFromParcel(Parcel in) {
            return new ConecktaPaymentResponseModel(in);
        }

        @Override
        public ConecktaPaymentResponseModel[] newArray(int size) {
            return new ConecktaPaymentResponseModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public String getNumberOfTickets() {
        return numberOfTickets;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public String getRecepit() {
        return recepit;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getInPersonCourseId() {
        return inPersonCourseId;
    }

    public String getId() {
        return id;
    }

    String status;
}
