package com.medialima.zertu.model;

import java.util.ArrayList;

/**
 * Created by Mohan M on 5/15/2018.
 */

public class CategoryDetailResponseModel {
    String id,name,description,galleryUuid,coverImageUrl,images,primaryColor,iconUrl,contentsTitle;
    ArrayList<ContentsModel> contents;
    ArrayList<CoursesModel> courses;
    ArrayList<MastersModel> masters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGalleryUuid() {
        return galleryUuid;
    }

    public void setGalleryUuid(String galleryUuid) {
        this.galleryUuid = galleryUuid;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getContentsTitle() {
        return contentsTitle;
    }

    public void setContentsTitle(String contentsTitle) {
        this.contentsTitle = contentsTitle;
    }

    public ArrayList<ContentsModel> getContents() {
        return contents;
    }

    public void setContents(ArrayList<ContentsModel> contents) {
        this.contents = contents;
    }

    public ArrayList<CoursesModel> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<CoursesModel> courses) {
        this.courses = courses;
    }

    public ArrayList<MastersModel> getMasters() {
        return masters;
    }

    public void setMasters(ArrayList<MastersModel> masters) {
        this.masters = masters;
    }
}
