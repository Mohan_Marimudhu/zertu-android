package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/15/2018.
 */

public class PaypalResponseModel {
    String id;
    String inPersonCourseId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInPersonCourseId() {
        return inPersonCourseId;
    }

    public void setInPersonCourseId(String inPersonCourseId) {
        this.inPersonCourseId = inPersonCourseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecepit() {
        return recepit;
    }

    public void setRecepit(String recepit) {
        this.recepit = recepit;
    }

    public String getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(String numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String name;
    String recepit;
    String numberOfTickets;
    String email;
    String amountPaid;
    String paidBy;
    String status;
}
