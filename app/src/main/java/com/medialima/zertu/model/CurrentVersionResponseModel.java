package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/28/2018.
 */

public class CurrentVersionResponseModel {
    String minAndroidVersion,minIosVersion;
    String androidUrl,iosUrl;

    public String getMinAndroidVersion() {
        return minAndroidVersion;
    }

    public String getMinIosVersion() {
        return minIosVersion;
    }

    public String getAndroidUrl() {
        return androidUrl;
    }

    public String getIosUrl() {
        return iosUrl;
    }
}
