package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 7/4/2018.
 */

public class UserTicketsModel implements Parcelable {
    String id,inPersonCourseId,name,email,recepit,amountPaid,numberOfTickets,paidBy,status;
    InPersonCourseModel inPersonCourse;

    protected UserTicketsModel(Parcel in) {
        id = in.readString();
        inPersonCourseId = in.readString();
        name = in.readString();
        email = in.readString();
        recepit = in.readString();
        amountPaid = in.readString();
        numberOfTickets = in.readString();
        paidBy = in.readString();
        status = in.readString();
        inPersonCourse = in.readParcelable(InPersonCourseModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(inPersonCourseId);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(recepit);
        dest.writeString(amountPaid);
        dest.writeString(numberOfTickets);
        dest.writeString(paidBy);
        dest.writeString(status);
        dest.writeParcelable(inPersonCourse, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserTicketsModel> CREATOR = new Creator<UserTicketsModel>() {
        @Override
        public UserTicketsModel createFromParcel(Parcel in) {
            return new UserTicketsModel(in);
        }

        @Override
        public UserTicketsModel[] newArray(int size) {
            return new UserTicketsModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getInPersonCourseId() {
        return inPersonCourseId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getRecepit() {
        return recepit;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public String getNumberOfTickets() {
        return numberOfTickets;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public String getStatus() {
        return status;
    }

    public InPersonCourseModel getInPersonCourse() {
        return inPersonCourse;
    }


}
