package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Mohan M on 5/15/2018.
 */

public class MastersModel implements Parcelable{
    String id;
    String name;
    String primaryPicUrl;
    String gallery;
    String galleryUuid;
    String description;


    protected MastersModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        primaryPicUrl = in.readString();
        gallery = in.readString();
        galleryUuid = in.readString();
        description = in.readString();
        categoryDescription = in.readString();
        courses = in.createTypedArrayList(CoursesModel.CREATOR);
    }

    public static final Creator<MastersModel> CREATOR = new Creator<MastersModel>() {
        @Override
        public MastersModel createFromParcel(Parcel in) {
            return new MastersModel(in);
        }

        @Override
        public MastersModel[] newArray(int size) {
            return new MastersModel[size];
        }
    };

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    String categoryDescription;
    ArrayList<CoursesModel> courses;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrimaryPicUrl() {
        return primaryPicUrl;
    }

    public void setPrimaryPicUrl(String primaryPicUrl) {
        this.primaryPicUrl = primaryPicUrl;
    }

    public String getGallery() {
        return gallery;
    }

    public void setGallery(String gallery) {
        this.gallery = gallery;
    }

    public String getGalleryUuid() {
        return galleryUuid;
    }

    public void setGalleryUuid(String galleryUuid) {
        this.galleryUuid = galleryUuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<CoursesModel> getCourses() {
        return courses;
    }

    public void setCourses(ArrayList<CoursesModel> courses) {
        this.courses = courses;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(primaryPicUrl);
        parcel.writeString(gallery);
        parcel.writeString(galleryUuid);
        parcel.writeString(description);
        parcel.writeString(categoryDescription);
        parcel.writeTypedList(courses);
    }
}
