package com.medialima.zertu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohan M on 7/4/2018.
 */

public class InPersonCourseModel implements Parcelable{
    String id;
    String title;
    String description;
    String price;
    String maxCapacity;
    String coverImageUrl;
    String instructionsFileUrl;
    String latitude;
    String startDate;

    public String getEndDate() {
        return endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    String endDate;

    protected InPersonCourseModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        description = in.readString();
        price = in.readString();
        maxCapacity = in.readString();
        coverImageUrl = in.readString();
        instructionsFileUrl = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        startDate=in.readString();
        endDate=in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(price);
        dest.writeString(maxCapacity);
        dest.writeString(coverImageUrl);
        dest.writeString(instructionsFileUrl);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(startDate);
        dest.writeString(endDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InPersonCourseModel> CREATOR = new Creator<InPersonCourseModel>() {
        @Override
        public InPersonCourseModel createFromParcel(Parcel in) {
            return new InPersonCourseModel(in);
        }

        @Override
        public InPersonCourseModel[] newArray(int size) {
            return new InPersonCourseModel[size];
        }
    };

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getInstructionsFileUrl() {
        return instructionsFileUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public String getMaxCapacity() {
        return maxCapacity;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    String longitude;


}
