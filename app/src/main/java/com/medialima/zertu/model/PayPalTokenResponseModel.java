package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/11/2018.
 */

public class PayPalTokenResponseModel {
    int code;
    String response;

    public int getCode() {
        return code;
    }

    public String getResponse() {
        return response;
    }
}
