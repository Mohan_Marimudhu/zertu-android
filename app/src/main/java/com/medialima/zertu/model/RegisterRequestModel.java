package com.medialima.zertu.model;

/**
 * Created by Mohan M on 5/18/2018.
 */

public class RegisterRequestModel {
    String email;
    String username;
    String name;
    String password;
    String address_line_1;

    public void setAddress_line_2(String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    String address_line_2;
}
