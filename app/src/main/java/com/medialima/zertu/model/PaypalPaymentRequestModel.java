package com.medialima.zertu.model;

/**
 * Created by Mohan M on 6/15/2018.
 */

public class PaypalPaymentRequestModel {
    String inPersonCourseId;
    String numberOfPeople;

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public void setNumberOfPeople(String numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public void setInPersonCourseId(String inPersonCourseId) {
        this.inPersonCourseId = inPersonCourseId;
    }

    String cardToken;
}
