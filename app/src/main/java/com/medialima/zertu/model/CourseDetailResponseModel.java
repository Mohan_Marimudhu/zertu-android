package com.medialima.zertu.model;

import java.util.ArrayList;

/**
 * Created by Mohan M on 5/18/2018.
 */

public class CourseDetailResponseModel {
    String id;
    String coverImageUrl;
    String categoryId;
    String description;
    String primaryColor;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

//    public ArrayList<MastersModel> getMasters() {
//        return masters;
//    }
//
//    public void setMasters(ArrayList<MastersModel> masters) {
//        this.masters = masters;
//    }

    public ArrayList<VideosModel> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<VideosModel> videos) {
        this.videos = videos;
    }

//    public ArrayList<CategoryModel> getCategory() {
//        return category;
//    }
//
//    public void setCategory(ArrayList<CategoryModel> category) {
//        this.category = category;
//    }

    String brief;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;
//    ArrayList<MastersModel> masters;
//    ArrayList<CategoryModel> category;
    ArrayList<VideosModel> videos;

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    CategoryModel category;
}
