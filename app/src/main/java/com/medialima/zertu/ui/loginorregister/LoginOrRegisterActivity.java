package com.medialima.zertu.ui.loginorregister;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;

import com.medialima.zertu.R;
import com.medialima.zertu.ui.home.MainActivity;
import com.medialima.zertu.utils.UserUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.ButterKnife;

public class LoginOrRegisterActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (UserUtils.isLoggedIn(getApplicationContext())){
            moveToHomePage();
        }
        setContentView(R.layout.activity_login_or_register);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        getHashKey();
        loadLoginOrRegisterFragment();

    }

    private void moveToHomePage() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void getHashKey() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.medialima.zertu", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
                //Toast.makeText(getApplicationContext(), sign, Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    private void loadLoginOrRegisterFragment() {
        LoginOrRegisterFragment loginOrRegisterFragment = new LoginOrRegisterFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, loginOrRegisterFragment);
        fragmentTransaction.commit();
    }


}
