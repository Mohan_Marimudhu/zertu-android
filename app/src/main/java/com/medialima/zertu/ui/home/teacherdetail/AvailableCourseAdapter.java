package com.medialima.zertu.ui.home.teacherdetail;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.CoursesModel;
import com.medialima.zertu.ui.home.coursedetail.CourseDetailActivity;
import com.medialima.zertu.utils.Constants;

import java.util.ArrayList;

/**
 * Created by Mohan M on 5/16/2018.
 */

public class AvailableCourseAdapter extends RecyclerView.Adapter<AvailableCourseAdapter.ViewHolder> {
    ArrayList<CoursesModel> courses;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView courseNameTextView;
        public ViewHolder(View v) {
            super(v);
            courseNameTextView=(TextView)v.findViewById(R.id.courseNameTextView);
        }
    }

    public AvailableCourseAdapter(Context context, ArrayList<CoursesModel> courses) {
        this.context=context;
        this.courses=courses;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AvailableCourseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.available_course_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AvailableCourseAdapter.ViewHolder holder, final int position) {
        holder.courseNameTextView.setText(courses.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, CourseDetailActivity.class);
                intent.putExtra(Constants.EXTRAS_COURSE_ID,courses.get(position).getId());
                intent.putExtra(Constants.EXTRAS_NAME,courses.get(position).getName());
                context.startActivity(intent);
            }
        });
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return courses.size();
    }
}
