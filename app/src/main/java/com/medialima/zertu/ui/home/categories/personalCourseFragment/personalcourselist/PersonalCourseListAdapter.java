package com.medialima.zertu.ui.home.categories.personalCourseFragment.personalcourselist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.medialima.zertu.R;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.ui.home.categories.categorydetail.CategoryDetailPage;
import com.medialima.zertu.ui.home.categories.personalCourseFragment.personalcoursedetail.PersonalCourseDetailActivity;
import com.medialima.zertu.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Mohan M on 6/12/2018.
 */

public class PersonalCourseListAdapter extends RecyclerView.Adapter<PersonalCourseListAdapter.ViewHolder> {
    private ArrayList<PersonalCourseListModel> data;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView relatedCategoryImageView;
        TextView titleTextView, descriptionTextView;
        public ViewHolder(View v) {
            super(v);
            relatedCategoryImageView=(RoundedImageView)v.findViewById(R.id.relatedCategoryImageView);
            titleTextView=(TextView)v.findViewById(R.id.tittleTextView);
            descriptionTextView=(TextView)v.findViewById(R.id.descriptionTextView);
        }
    }

    public PersonalCourseListAdapter(Context context,ArrayList<PersonalCourseListModel> data) {
        this.context=context;
        this.data=data;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PersonalCourseListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.related_category_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PersonalCourseListAdapter.ViewHolder holder, final int position) {
        if (data.get(position).getCoverImageUrl()==null){
            Picasso.with(context).load(R.drawable.cursos_presenciales).into(holder.relatedCategoryImageView);
        }else {
            try {
                Picasso.with(context).load(Constants.urlEncode(data.get(position).getCoverImageUrl())).fit().into(holder.relatedCategoryImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        holder.titleTextView.setText(data.get(position).getTitle());
        holder.descriptionTextView.setText(Html.fromHtml(data.get(position).getDescription()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, PersonalCourseDetailActivity.class);
                intent.putExtra(Constants.EXTRAS_COURSE_ID,data.get(position).getId());
                intent.putExtra(Constants.EXTRAS_NAME,data.get(position).getTitle());
                context.startActivity(intent);
            }
        });
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }
}