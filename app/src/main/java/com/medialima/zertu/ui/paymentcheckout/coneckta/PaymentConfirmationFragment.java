package com.medialima.zertu.ui.paymentcheckout.coneckta;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.ConecktaPaymentResponseModel;
import com.medialima.zertu.model.PersonalCourseListModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PaymentConfirmationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaymentConfirmationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    PersonalCourseListModel data;
    String status;
    String noOfTickets;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.paymentStatusTextView)
    TextView paymentStatusTextView;
    @BindView(R.id.courseNameTextView)
    TextView courseNameTextView;
    @BindView(R.id.priceTextView)
    TextView priceTextView;
    @BindView(R.id.numberOfTicketTextView)
    TextView numberOfTicketTextView;
    @BindView(R.id.totalTextView)
    TextView totalTextView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PaymentConfirmationFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PaymentConfirmationFragment newInstance(PersonalCourseListModel data,String noOfTickets,String status) {
        PaymentConfirmationFragment fragment = new PaymentConfirmationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, data);
        args.putString(ARG_PARAM2,noOfTickets);
        args.putString(ARG_PARAM3,status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = getArguments().getParcelable(ARG_PARAM1);
            noOfTickets=getArguments().getString(ARG_PARAM2);
            status=getArguments().getString(ARG_PARAM3);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_confirmation, container, false);
        ButterKnife.bind(this, view);
        setUpData();
        return view;
    }

    private void setUpData() {
        if (status.equals("1")){
            imageView.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.check_icon));
            paymentStatusTextView.setText(getString(R.string.payment_sucess_message));

        }else {
            imageView.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.cancel_icon));
            paymentStatusTextView.setText(getString(R.string.payment_faliure_message));
        }
        courseNameTextView.setText(" "+data.getTitle());
        priceTextView.setText(" $"+data.getPrice());
        double total=Double.valueOf(" "+data.getPrice())*Double.valueOf(noOfTickets);
        totalTextView.setText(" $"+total+"");
        numberOfTicketTextView.setText(" "+noOfTickets+"");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
