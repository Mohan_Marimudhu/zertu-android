package com.medialima.zertu.ui.home.categories.categorydetail;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.medialima.zertu.R;
import com.medialima.zertu.model.CategoryDetailResponseModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CategoryDeatailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryDeatailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tittleTextView)
    TextView tittleTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.availableTeachersRecyclerView)
    RecyclerView availableTeachersRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    String categoryId;
    @BindView(R.id.availableTeacherTextView)
    TextView availableTeacherTextView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CategoryDeatailFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CategoryDeatailFragment newInstance(String categoryID) {
        CategoryDeatailFragment fragment = new CategoryDeatailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, categoryID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category_deatail, container, false);
        ButterKnife.bind(this, view);
        if (categoryId!=null){
            getCategoryDetail();
        }
        return view;
    }

    private void getCategoryDetail() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(
                    getContext()).findCategoryById(categoryId,
                    new ResponseCallback<GenericResponse<CategoryDetailResponseModel>>(getContext()) {
                        @Override
                        public void onResponse(GenericResponse<CategoryDetailResponseModel> response) {
                            hideProgressBar();
                            if (response.getStatus() == Constants.RESPONSE_OK) {
                                setUpData(response.getData());
                            }
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpData(CategoryDetailResponseModel data) {
        try {
            Picasso.with(getContext()).load(Constants.urlEncode(data.getCoverImageUrl())).into(imageView);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        tittleTextView.setText(data.getName());
        descriptionTextView.setText(Html.fromHtml(data.getDescription()));
//        SETUP RECYCLER VIEW
        if (data.getMasters()!=null&&data.getMasters().size()!=0) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            availableTeachersRecyclerView.setLayoutManager(layoutManager);
            availableTeachersRecyclerView.setNestedScrollingEnabled(false);
            AvailableTeacherAdapter availableTeacherAdapter = new AvailableTeacherAdapter(getContext(), data.getMasters());
            availableTeachersRecyclerView.setAdapter(availableTeacherAdapter);
        }else {
            availableTeacherTextView.setVisibility(View.GONE);
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
