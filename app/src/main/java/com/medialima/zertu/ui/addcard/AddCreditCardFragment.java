package com.medialima.zertu.ui.addcard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.model.CreatePaymentRequestModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.conekta.conektasdk.Card;
import io.conekta.conektasdk.Conekta;
import io.conekta.conektasdk.Token;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddCreditCardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCreditCardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int MY_SCAN_REQUEST_CODE = 1;
    @BindView(R.id.scanLayout)
    LinearLayout scanLayout;
    @BindView(R.id.addCreditCard)
    RelativeLayout addCreditCard;
    @BindView(R.id.card_number)
    EditText mCardNumber;
    @BindView(R.id.name)
    EditText mName;
    @BindView(R.id.monthAndYear)
    EditText monthAndYearEditText;
    @BindView(R.id.cvv)
    EditText mCvv;
    @BindView(R.id.scanTextView)
    TextView scanTextView;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AddCreditCardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddCreditCardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddCreditCardFragment newInstance(String param1, String param2) {
        AddCreditCardFragment fragment = new AddCreditCardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_credit_card, container, false);
        ButterKnife.bind(this, view);

        mCardNumber.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int pos = 0;
                while (true) {
                    if (pos >= editable.length()) break;
                    if (space == editable.charAt(pos) && (((pos + 1) % 5) != 0 || pos + 1 == editable.length())) {
                        editable.delete(pos, pos + 1);
                    } else {
                        pos++;
                    }
                }

                // Insert char where needed.
                pos = 4;
                while (true) {
                    if (pos >= editable.length()) break;
                    final char c = editable.charAt(pos);
                    // Only if its a digit where there should be a space we insert a space
                    if ("0123456789".indexOf(c) >= 0) {
                        editable.insert(pos, "" + space);
                    }
                    pos += 5;
                }
            }
        });

        SimpleDateFormat formatter =
                new SimpleDateFormat("MM/yy", Locale.GERMANY);
        Calendar expiryDateDate = Calendar.getInstance();
        try {
            expiryDateDate.setTime(formatter.parse(monthAndYearEditText.getText().toString()));
        } catch (ParseException e) {
            //not valid
        }

        monthAndYearEditText.addTextChangedListener(new TextWatcher() {
            String mLastInput = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                String input = editable.toString();
                SimpleDateFormat formatter = new SimpleDateFormat("MM/yy", Locale.GERMANY);
                Calendar expiryDateDate = Calendar.getInstance();
                try {
                    expiryDateDate.setTime(formatter.parse(input));

                } catch (ParseException e) {
                    try {


                        if (editable.length() == 2 && !mLastInput.endsWith("/")) {
                            int month = Integer.parseInt(input);
                            if (month <= 12) {
                                monthAndYearEditText.setText(monthAndYearEditText.getText().toString() + "/");
                                monthAndYearEditText.setSelection(monthAndYearEditText.getText().toString().length());
                            }
                        } else if (editable.length() == 2 && mLastInput.endsWith("/")) {
                            int month = Integer.parseInt(input);
                            if (month <= 12) {
                                monthAndYearEditText.setText(monthAndYearEditText.getText().toString().substring(0, 1));
                                monthAndYearEditText.setSelection(monthAndYearEditText.getText().toString().length());
                            } else {
                                monthAndYearEditText.setText("");
                                monthAndYearEditText.setSelection(monthAndYearEditText.getText().toString().length());
                                Toast.makeText(getContext(), "Enter a valid month", Toast.LENGTH_LONG).show();
                            }
                        } else if (editable.length() == 1) {
                            int month = Integer.parseInt(input);
                            if (month > 1) {
                                monthAndYearEditText.setText("0" + monthAndYearEditText.getText().toString() + "/");
                                monthAndYearEditText.setSelection(monthAndYearEditText.getText().toString().length());
                            }
                        } else {

                        }
                    }catch (NumberFormatException e1){
                        e.printStackTrace();
                    }
                    mLastInput = monthAndYearEditText.getText().toString();
                    return;
                }
            }
        });

        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {

                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                String expiryYear=scanResult.expiryYear+"";
                String expiryMonth=scanResult.expiryMonth+"";
                Log.w("Expiry year", expiryYear.substring(2,4));
                if(Integer.parseInt(expiryMonth)<10)
                {
                    expiryMonth="0"+expiryMonth;
                }
                setValuesToView(scanResult.getFormattedCardNumber(), expiryMonth + "/" +  expiryYear.substring(2,4), scanResult.cvv, scanResult.cardholderName);
            } else {

            }

        }

    }

    private void setValuesToView(String redactedCardNumber, String expiryDate, String cvv, String cardholderName) {
        mCardNumber.setText(redactedCardNumber);
        monthAndYearEditText.setText(expiryDate);
        mCvv.setText(cvv);
        mName.setText(cardholderName);

    }
    private boolean isFormValid() {
        String name = mName.getText().toString();
        String cardNumber = mCardNumber.getText().toString();
        String cvv = mCvv.getText().toString();
        String[] expireDate = monthAndYearEditText.getText().toString().split("/");

        boolean isNameValid = true, isCardValid = true, isCvvValid = true, isMonthAndYearValid = true;
        if (TextUtils.isEmpty(name)) {
            isNameValid = false;
            mName.setError(getString(R.string.enter_name));
        }
        if (cardNumber.length() < 19) {
            isCardValid = false;
            mCardNumber.setError(getString(R.string.enter_card_number));
        }

        if (cvv.length() < 3) {
            isCvvValid = false;
            mCvv.setError(getString(R.string.enter_cvv));
        }
        if (monthAndYearEditText.getText().length() == 0 && monthAndYearEditText.getText().length() < 7) {
            monthAndYearEditText.setError(getResources().getString(R.string.entert_the_valid_expiry_month_and_year));
            isMonthAndYearValid = false;
        }

        return (isNameValid && isCardValid && isCvvValid && isMonthAndYearValid);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.scanLayout, R.id.addCreditCard})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scanLayout:
                Intent scanIntent = new Intent(view.getContext(), CardIOActivity.class);

                // customize these values to suit your needs.
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
                scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true);

                // MY_SCAN_REQUEST_CODE is arbitrary and is only used within this activity.
                startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
                break;
            case R.id.addCreditCard:
                if (InternetAvailability.isNetworkAvailable(getActivity())) {
                    if (isFormValid())
                        addConecktaPaymentMethod();
                } else {
                    showNoInternetDialog(getActivity());
                }
                break;
        }
    }

    public void showNoInternetDialog(Context context) {
        showAlert(context, getString(R.string.no_internet),
                getString(R.string.please_check_internet),
                getString(R.string.ok), null, true, null);
    }
    private void showPaymentConfimationDialog(final String token) {
        String amount="";
        new AlertDialog.Builder(getContext())
                .setTitle(getResources().getString(R.string.payment))
                .setMessage(getResources().getString(R.string.are_you_sure_want_to_pay) + amount)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        payByUsingConeckta(token);
                        dialog.cancel();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        hideProgress();
                    }
                })
                .setCancelable(false)
                .show();
    }
    private void addConecktaPaymentMethod() {
        showProgress();
        try {

            Conekta.setPublicKey(getString(R.string.conekta_production_key)); //Set public key
            Conekta.collectDevice(getActivity()); //Collect device

            String[] expireDate = monthAndYearEditText.getText().toString().split("/");
            String month = expireDate[0];
            String year = expireDate[1];
            Card card = new Card(mName.getText().toString(), mCardNumber.getText().toString(),
                    mCvv.getText().toString(), month, year);
            Token token = new Token(getActivity());

            token.onCreateTokenListener(new Token.CreateToken() {
                @Override
                public void onCreateTokenReady(JSONObject data) {
                    if (!isAdded()) return;
                    try {
                        String token = data.getString("id");
                        saveCreditCard(token);
                          //  showPaymentConfimationDialog(token);
                    } catch (Exception err) {
                        hideProgress();
                        showAlert(getActivity(), getString(R.string.error), getString(R.string.please_check_your_card), getString(R.string.ok),
                                null, true, null);
                        Log.d("Error: ", err.toString());
                    }
                }
            });

            token.create(card);
        } catch (RuntimeException e) {
            e.printStackTrace();
            hideProgress();
            Toast.makeText(getContext(), getResources().getString(R.string.please_enter_the_valid_expiry_information), Toast.LENGTH_SHORT).show();
        }
    }

    private void saveCreditCard(String token) {
        if (InternetAvailability.isNetworkAvailable(getContext())){
            CreatePaymentRequestModel createPaymentRequestModel=new CreatePaymentRequestModel();
            createPaymentRequestModel.setPaymentToken(token);
            showProgress();
            NetworkAdapter.getInstance(getContext()).
                    createPaymentRequestModel(createPaymentRequestModel, new ResponseCallback<GenericResponse>(getContext()) {
                @Override
                public void onResponse(GenericResponse response) {
                    if (response.getStatus()== 201){
                        hideProgress();
                        getActivity().finish();
                    }

                }

                @Override
                public void onFailure() {
                    hideProgress();
                }
            });
        }else {
            Toast.makeText(getContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }


    private void payByUsingConeckta(final String token) {
        showProgress();

    }


    protected void showAlert(Context context,
                             String title,
                             String message,
                             String positiveActionText,
                             String negativeActionText,
                             boolean isCancelable,
                             final DialogListener dialogListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        if (!TextUtils.isEmpty(title))
            builder.setTitle(title);
        if (!TextUtils.isEmpty(positiveActionText)) {
            builder.setPositiveButton(positiveActionText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (dialogListener != null)
                        dialogListener.onPositiveAction();
                }
            });
        }
        if (!TextUtils.isEmpty(negativeActionText)) {
            builder.setNegativeButton(negativeActionText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (dialogListener != null)
                        dialogListener.onNegativeAction();
                }
            });
        }
        builder.setCancelable(isCancelable);
        builder.show();
    }
    private void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }
    private void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
