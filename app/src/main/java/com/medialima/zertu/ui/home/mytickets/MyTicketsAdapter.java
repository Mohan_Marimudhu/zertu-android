package com.medialima.zertu.ui.home.mytickets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.UserTicketsModel;
import com.medialima.zertu.utils.Constants;

import java.util.ArrayList;

/**
 * Created by Mohan M on 7/4/2018.
 */

public class MyTicketsAdapter extends RecyclerView.Adapter<MyTicketsAdapter.ViewHolder> {
    ArrayList<UserTicketsModel> userTicketsModelArrayList;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView courseNameTextView,numberOfTicketsTextView,priceTextView;
        public ViewHolder(View v) {
            super(v);
            courseNameTextView=(TextView)v.findViewById(R.id.courseNameTextView);
            numberOfTicketsTextView=(TextView)v.findViewById(R.id.numberOfTicketTextView);
            priceTextView=(TextView)v.findViewById(R.id.priceTextView);
        }
    }

    public MyTicketsAdapter(Context context, ArrayList<UserTicketsModel> userTicketsModelArrayList) {
        this.context=context;
        this.userTicketsModelArrayList=userTicketsModelArrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyTicketsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tickets_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyTicketsAdapter.ViewHolder holder, final int position) {
        holder.courseNameTextView.setText(userTicketsModelArrayList.get(position).getInPersonCourse().getTitle());
        holder.numberOfTicketsTextView.setText("Número de boletos: "+userTicketsModelArrayList.get(position).getNumberOfTickets());
        holder.priceTextView.setText("Total pagado: $"+userTicketsModelArrayList.get(position).getAmountPaid());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,TicketDetailActivity.class);
                intent.putExtra(Constants.EXTRAS_TICKET,userTicketsModelArrayList.get(position));
                context.startActivity(intent);
            }
        });
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return userTicketsModelArrayList.size();
    }
}
