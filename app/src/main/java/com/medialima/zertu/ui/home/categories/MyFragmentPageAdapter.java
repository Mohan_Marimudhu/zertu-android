package com.medialima.zertu.ui.home.categories;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.medialima.zertu.model.AllCategoryResponseModel;
import com.medialima.zertu.ui.home.categories.pager.PagerFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohan M on 4/4/2018.
 */

public class MyFragmentPageAdapter extends FragmentStatePagerAdapter {
    public static int LOOPS_COUNT = 500;
    ArrayList<AllCategoryResponseModel> data;
    private List<Fragment> myFragments;
    private Context context;
    FragmentManager fragmentManager;

    public MyFragmentPageAdapter(Context context, FragmentManager fragmentManager, List<Fragment> myFrags, ArrayList<AllCategoryResponseModel> data) {
        super(fragmentManager);
        myFragments = myFrags;
        this.context = context;
        this.fragmentManager=fragmentManager;
        this.data=data;

    }

    @Override
    public Fragment getItem(int position) {
        if (data.size()>0&&data!=null){
            position = position % data.size(); // use modulo for infinite cycling
            return PagerFragment.newInstance(data.get(position));
        }else {
            return PagerFragment.newInstance(null);
        }
//        return PagerFragment.newInstance(data.get(position));
    }


    @Override
    public int getCount() {
        if (data != null && data.size() > 0)
        {
            return data.size()*LOOPS_COUNT; // simulate infinite by big number of products
        }
        else
        {
            return 1;
        }
//        return myFragments.size();
    }



}