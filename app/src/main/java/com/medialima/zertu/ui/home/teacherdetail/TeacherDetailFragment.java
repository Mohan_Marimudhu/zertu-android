package com.medialima.zertu.ui.home.teacherdetail;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.medialima.zertu.R;
import com.medialima.zertu.model.MastersModel;
import com.medialima.zertu.utils.Constants;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TeacherDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeacherDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tittleTextView)
    TextView tittleTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.availableCourseRecyclerView)
    RecyclerView availableCourseRecyclerView;
    @BindView(R.id.availableCourseTextView)
    TextView availableCourseTextView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    MastersModel teacher;
    private OnFragmentInteractionListener mListener;

    public TeacherDetailFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TeacherDetailFragment newInstance(MastersModel teacher) {
        TeacherDetailFragment fragment = new TeacherDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, teacher);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            teacher = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teacher_detail, container, false);
        ButterKnife.bind(this, view);
        setUpData();
        return view;
    }

    private void setUpData() {
        try {
            Picasso.with(getContext()).load(Constants.urlEncode(teacher.getPrimaryPicUrl())).into(imageView);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        tittleTextView.setText(teacher.getName());
        descriptionTextView.setText(Html.fromHtml(teacher.getDescription()));

//        Setup Adapter
        if (teacher.getCourses() != null && teacher.getCourses().size() != 0) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            availableCourseRecyclerView.setLayoutManager(layoutManager);
            availableCourseRecyclerView.setNestedScrollingEnabled(false);
            AvailableCourseAdapter availableCourseAdapter = new AvailableCourseAdapter(getContext(), teacher.getCourses());
            availableCourseRecyclerView.setAdapter(availableCourseAdapter);
        }else {
            availableCourseTextView.setVisibility(View.GONE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
