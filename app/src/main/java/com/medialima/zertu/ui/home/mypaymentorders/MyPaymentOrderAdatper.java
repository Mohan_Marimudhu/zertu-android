package com.medialima.zertu.ui.home.mypaymentorders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.OxxoRequestModel;
import com.medialima.zertu.model.UserTicketsModel;
import com.medialima.zertu.ui.home.mytickets.TicketDetailActivity;
import com.medialima.zertu.ui.paymentcheckout.oxxo.OxxoPaymentConfimationActivity;
import com.medialima.zertu.utils.Constants;

import java.util.ArrayList;

/**
 * Created by Mohan M on 7/4/2018.
 */

public class MyPaymentOrderAdatper extends RecyclerView.Adapter<MyPaymentOrderAdatper.ViewHolder> {
    ArrayList<OxxoRequestModel> oxxoRequestModelArrayList;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView courseNameTextView,numberOfTicketsTextView,priceTextView;
        public ViewHolder(View v) {
            super(v);
            courseNameTextView=(TextView)v.findViewById(R.id.courseNameTextView);
            numberOfTicketsTextView=(TextView)v.findViewById(R.id.numberOfTicketTextView);
            priceTextView=(TextView)v.findViewById(R.id.priceTextView);
        }
    }

    public MyPaymentOrderAdatper(Context context,  ArrayList<OxxoRequestModel> oxxoRequestModelArrayList) {
        this.context=context;
        this.oxxoRequestModelArrayList=oxxoRequestModelArrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyPaymentOrderAdatper.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_payment_order_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        MyPaymentOrderAdatper.ViewHolder vh = new MyPaymentOrderAdatper.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyPaymentOrderAdatper.ViewHolder holder, final int position) {
        holder.courseNameTextView.setText(oxxoRequestModelArrayList.get(position).getTitle());
        holder.numberOfTicketsTextView.setText("Número de boletos: "+oxxoRequestModelArrayList.get(position).getNumberOfTickets());
        holder.priceTextView.setText("Total pagado: $"+oxxoRequestModelArrayList.get(position).getTotalAmount());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, OxxoPaymentConfimationActivity.class);
                intent.putExtra(Constants.OXXO_REQUEST,oxxoRequestModelArrayList.get(position));
                context.startActivity(intent);
            }
        });
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return oxxoRequestModelArrayList.size();
    }
}
