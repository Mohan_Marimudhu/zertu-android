package com.medialima.zertu.ui.paymentcheckout.coneckta;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConecktaCheckoutActivity extends AppCompatActivity {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;
    PersonalCourseListModel data;
    String numberOfTickets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coneckta_checkout);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        data=getIntent().getParcelableExtra(Constants.EXTRAS_PERSONAL_COURSES_DETAIL);
        numberOfTickets=getIntent().getStringExtra(Constants.EXTRA_NUMBER_OF_TICKETS);
        loadConecktaCheckoutFragment();
    }

    private void loadConecktaCheckoutFragment() {
        ConecktaCheckoutFragment conecktaCheckoutFragment =ConecktaCheckoutFragment.newInstance(data,numberOfTickets);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, conecktaCheckoutFragment);
        fragmentTransaction.commit();
    }

    public void loadPaymentConfirmationPage(PersonalCourseListModel data, String numberOfTickets,String status) {
        PaymentConfirmationFragment paymentConfirmationFragment =PaymentConfirmationFragment.newInstance(data,numberOfTickets,status);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, paymentConfirmationFragment);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.backImageView)
    public void onClick() {
        super.onBackPressed();
    }
}
