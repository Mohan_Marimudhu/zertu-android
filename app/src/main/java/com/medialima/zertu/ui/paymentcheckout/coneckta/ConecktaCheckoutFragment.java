package com.medialima.zertu.ui.paymentcheckout.coneckta;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.common.RecyclerItemClickListener;
import com.medialima.zertu.model.CardModel;
import com.medialima.zertu.model.ConecktaPaymentResponseModel;
import com.medialima.zertu.model.CreatePaymentRequestModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.PaymentRequestModel;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.addcard.AddCreditCardActivity;
import com.medialima.zertu.ui.home.myprofile.CardListAdapter;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConecktaCheckoutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConecktaCheckoutFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static  int REQUEST_CODE=100;
    @BindView(R.id.courseNameTextView)
    TextView courseNameTextView;
    @BindView(R.id.priceTextView)
    TextView priceTextView;
    @BindView(R.id.numberOfTicketTextView)
    TextView numberOfTicketTextView;
    @BindView(R.id.totalTextView)
    TextView totalTextView;
    @BindView(R.id.caredListRecyclerView)
    RecyclerView caredListRecyclerView;
    @BindView(R.id.payButton)
    RelativeLayout payButton;
    PersonalCourseListModel data;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.addCreditCardButton)
    RelativeLayout addCreditCardButton;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ArrayList<CardModel> cardModelArrayList;
    private OnFragmentInteractionListener mListener;
    String numberOfTickets;

    public ConecktaCheckoutFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ConecktaCheckoutFragment newInstance(PersonalCourseListModel data, String numberOfTickets) {
        ConecktaCheckoutFragment fragment = new ConecktaCheckoutFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, data);
        args.putString(ARG_PARAM2, numberOfTickets);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = getArguments().getParcelable(ARG_PARAM1);
            numberOfTickets = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coneckta_checkout, container, false);
        ButterKnife.bind(this, view);
        getCardList();
        setUpData();
        caredListRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), caredListRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        changeDefaultPayment(position);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
        return view;
    }

    private void setUpData() {
        courseNameTextView.setText(" "+data.getTitle());
        priceTextView.setText(" $" + data.getPrice());
        numberOfTicketTextView.setText(" "+numberOfTickets + "");
        double total = Double.valueOf(" "+data.getPrice()) * Double.valueOf(numberOfTickets);
        totalTextView.setText(" $" + total);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void loadPaymentConfirmationPage(PersonalCourseListModel data,String numberOfTickets,String status) {
        ConecktaCheckoutActivity conecktaCheckoutActivity = (ConecktaCheckoutActivity) getContext();
        conecktaCheckoutActivity.loadPaymentConfirmationPage(data,numberOfTickets,status);
    }

    private void changeDefaultPayment(int position) {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            CreatePaymentRequestModel createPaymentRequestModel = new CreatePaymentRequestModel();
            createPaymentRequestModel.setPaymentToken(cardModelArrayList.get(position).getId());
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).
                    changeDefaultPayment(createPaymentRequestModel, new ResponseCallback<GenericResponse>(getContext()) {
                        @Override
                        public void onResponse(GenericResponse response) {
                            if (response.getStatus() == 201) {
                                hideProgressBar();
                                getCardList();
                                Toast.makeText(getContext(), getString(R.string.updated), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure() {
                            hideProgressBar();
                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }


    private void getCardList() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).
                    getCardList(new ResponseCallback<GenericResponse<ArrayList<CardModel>>>(getContext()) {
                        @Override
                        public void onResponse(GenericResponse<ArrayList<CardModel>> response) {
                            hideProgressBar();
                            if (response.getStatus() == Constants.RESPONSE_OK && response.getData() != null && response.getData().size() != 0) {
                                cardModelArrayList = response.getData();
                                setUpRecyclverView(response.getData());
                                addCreditCardButton.setVisibility(View.GONE);
                                payButton.setVisibility(View.VISIBLE);
                            }else {
                                addCreditCardButton.setVisibility(View.VISIBLE);
                                payButton.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure() {
                            hideProgressBar();
                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpRecyclverView(ArrayList<CardModel> data) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        caredListRecyclerView.setLayoutManager(layoutManager);
        caredListRecyclerView.setNestedScrollingEnabled(false);
        CardListAdapter cardListAdapter = new CardListAdapter(getContext(), data);
        caredListRecyclerView.setAdapter(cardListAdapter);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    @OnClick({R.id.payButton, R.id.addCreditCardButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.payButton:
                if (InternetAvailability.isNetworkAvailable(getContext())){
                    pay();
                }else {
                    Toast.makeText(getContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.addCreditCardButton:
                Intent intent=new Intent(getContext(), AddCreditCardActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
                break;
        }
    }

    private void pay() {
        showProgressBar();
        PaymentRequestModel paymentRequestModel=new PaymentRequestModel();
        paymentRequestModel.setInPersonCourseId(data.getId());
        paymentRequestModel.setNumberOfPeople(numberOfTickets);
        NetworkAdapter.getInstance(getContext()).
                payWithConeckta(paymentRequestModel, new ResponseCallback<GenericResponse<ConecktaPaymentResponseModel>>(getContext()) {
            @Override
            public void onResponse(GenericResponse<ConecktaPaymentResponseModel> response) {
                hideProgressBar();
                if (response.getStatus()==Constants.RESPONSE_OK){
                    loadPaymentConfirmationPage(data,numberOfTickets,"1");
                }else {
                    loadPaymentConfirmationPage(data,numberOfTickets,"0");
                }
            }

            @Override
            public void onFailure() {
                loadPaymentConfirmationPage(data,numberOfTickets,"0");
                hideProgressBar();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getCardList();
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
