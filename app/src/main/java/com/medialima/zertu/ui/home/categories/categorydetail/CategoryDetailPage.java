package com.medialima.zertu.ui.home.categories.categorydetail;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.ui.home.categories.personalCourseFragment.personalcourselist.PersonalCourseFragment;
import com.medialima.zertu.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CategoryDetailPage extends AppCompatActivity {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;
    String categoryId;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail_page);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        categoryId=getIntent().getStringExtra(Constants.EXTRAS_CATEGORY_ID);
        title=getIntent().getStringExtra(Constants.EXTRAS_NAME);
        tittleTexView.setText(title);
        if (categoryId==null){
            loadPersonalCourseFragment();
        }else {
            loadCategoryDetailFragment();
        }

    }

    private void loadPersonalCourseFragment() {
        PersonalCourseFragment personalCourseFragment=new PersonalCourseFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, personalCourseFragment);
        fragmentTransaction.commit();
    }

    private void loadCategoryDetailFragment() {
        CategoryDeatailFragment categoryDeatailFragment =CategoryDeatailFragment.newInstance(categoryId);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, categoryDeatailFragment);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.backImageView)
    public void onClick() {
        super.onBackPressed();
    }
}
