package com.medialima.zertu.ui.home.categories.personalCourseFragment.personalcoursedetail;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.BraintreeError;
import com.braintreepayments.api.exceptions.ErrorWithResponse;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.BraintreeCancelListener;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.interfaces.ConfigurationListener;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.Configuration;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.medialima.zertu.R;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.OXXOPaymentResponseModel;
import com.medialima.zertu.model.PayPalTokenResponseModel;
import com.medialima.zertu.model.PaymentRequestModel;
import com.medialima.zertu.model.PaypalPaymentRequestModel;
import com.medialima.zertu.model.PaypalResponseModel;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.paymentcheckout.coneckta.ConecktaCheckoutActivity;
import com.medialima.zertu.ui.paymentcheckout.oxxo.OxxoPaymentConfimationActivity;
import com.medialima.zertu.ui.paymentcheckout.paypal.PaypalCheckoutActivity;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonalCourseDetailActivity extends AppCompatActivity implements ConfigurationListener,
        PaymentMethodNonceCreatedListener, BraintreeErrorListener,BraintreeCancelListener {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tittleTextView)
    TextView tittleTextView;
    @BindView(R.id.descriptionTextView)
    WebView descriptionTextView;
    @BindView(R.id.priceTextView)
    TextView priceTextView;
    @BindView(R.id.minusIcon)
    ImageView minusIcon;
    @BindView(R.id.countTextView)
    TextView countTextView;
    @BindView(R.id.plusIcon)
    ImageView plusIcon;
    @BindView(R.id.creditCardPaymentButton)
    RelativeLayout creditCardPaymentButton;
    @BindView(R.id.paypalPaymentButton)
    RelativeLayout paypalPaymentButton;
    @BindView(R.id.oxxoPayment)
    RelativeLayout oxxoPayment;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    BraintreeFragment mBraintreeFragment;
    String tittle, id;
    PersonalCourseListModel data;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_course_detail);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tittle = getIntent().getStringExtra(Constants.EXTRAS_NAME);
        id = getIntent().getStringExtra(Constants.EXTRAS_COURSE_ID);
        tittleTexView.setText(tittle);

        getPersonalCouseDetail();

    }

    private void getPersonalCouseDetail() {
        if (InternetAvailability.isNetworkAvailable(getApplicationContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(getApplicationContext()).
                    getPersonalCourseDetail(id, new ResponseCallback<GenericResponse<PersonalCourseListModel>>(getApplicationContext()) {
                        @Override
                        public void onResponse(GenericResponse<PersonalCourseListModel> response) {
                            hideProgressBar();
                            setUpdata(response.getData());
                        }

                        @Override
                        public void onFailure() {
                            hideProgressBar();
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpdata(PersonalCourseListModel data) {
        this.data=data;
        if (data.getCoverImageUrl() == null) {
            Picasso.with(PersonalCourseDetailActivity.this).load(R.drawable.cursos_presenciales).into(imageView);
        } else {
            try {
                Picasso.with(PersonalCourseDetailActivity.this).load(Constants.urlEncode(data.getCoverImageUrl())).fit().into(imageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        tittleTextView.setText(data.getTitle());
        descriptionTextView.loadDataWithBaseURL(null, data.getDescription(), "text/html", "utf-8", null);
        priceTextView.setText("$" + data.getPrice() + " Por Persona");
    }

    public void setupBraintreeAndStartExpressCheckout() {
        int totalAmount=Double.valueOf(data.getPrice()).intValue()*Double.valueOf(countTextView.getText().toString()).intValue();
        Log.w("total amount",totalAmount+"");
        PayPalRequest request = new PayPalRequest(totalAmount+"")
                .currencyCode("MXN")
                .intent(PayPalRequest.INTENT_AUTHORIZE);
        PayPal.requestOneTimePayment(mBraintreeFragment, request);

    }

    private void requestPaypalToken() {
        if (InternetAvailability.isNetworkAvailable(PersonalCourseDetailActivity.this)) {
            showDialog();
            NetworkAdapter.getInstance(PersonalCourseDetailActivity.this).
                    paypalTokenReques(new ResponseCallback<PayPalTokenResponseModel>(PersonalCourseDetailActivity.this) {
                        @Override
                        public void onResponse(PayPalTokenResponseModel response) {
                            if (response.getCode() == Constants.RESPONSE_OK) {
                                try {
                                    mBraintreeFragment = BraintreeFragment.newInstance(PersonalCourseDetailActivity.this, response.getResponse());
                                    // mBraintreeFragment is ready to use!

                                    setupBraintreeAndStartExpressCheckout();


                                } catch (InvalidArgumentException e) {
                                    e.printStackTrace();
                                    // There was an issue with your authorization string.
                                }
                            }
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
        } else {
            Toast.makeText(PersonalCourseDetailActivity.this, getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onError(Exception error) {
        Log.w("Executed","YES");
        if (error instanceof ErrorWithResponse) {
            ErrorWithResponse errorWithResponse = (ErrorWithResponse) error;
            BraintreeError cardErrors = errorWithResponse.errorFor("creditCard");
            if (cardErrors != null) {
                // There is an issue with the credit card.
                BraintreeError expirationMonthError = cardErrors.errorFor("expirationMonth");
                if (expirationMonthError != null) {
                    // There is an issue with the expiration month.
                    Log.w("PAYPAL ERROR", expirationMonthError.getMessage());
                }
            }
        }
    }

    @Override
    public void onConfigurationFetched(Configuration configuration) {

    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        // Send nonce to server

        String nonce = paymentMethodNonce.getNonce();
        if(nonce!=null){
            payWithPaypal(nonce);
        }
//        Log.w("Nonce",nonce);
//        if (paymentMethodNonce instanceof PayPalAccountNonce) {
//            PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce) paymentMethodNonce;
//
//            // Access additional information
//            String email = payPalAccountNonce.getEmail();
//            String firstName = payPalAccountNonce.getFirstName();
//            String lastName = payPalAccountNonce.getLastName();
//            String phone = payPalAccountNonce.getPhone();
//
//
//            // See PostalAddress.java for details
//            PostalAddress billingAddress = payPalAccountNonce.getBillingAddress();
//            PostalAddress shippingAddress = payPalAccountNonce.getShippingAddress();
//
//        }
    }

    private void payWithPaypal(String nonce) {
        if (InternetAvailability.isNetworkAvailable(getApplicationContext())){
            PaypalPaymentRequestModel paypalPaymentRequestModel=new PaypalPaymentRequestModel();
            paypalPaymentRequestModel.setNumberOfPeople(countTextView.getText().toString());
            paypalPaymentRequestModel.setCardToken(nonce);
            paypalPaymentRequestModel.setInPersonCourseId(data.getId());
            NetworkAdapter.getInstance(getApplicationContext())
                    .payWithPaypal(paypalPaymentRequestModel, new ResponseCallback<GenericResponse<PaypalResponseModel>>(getApplicationContext()) {
                @Override
                public void onResponse(GenericResponse<PaypalResponseModel> response) {
                    cancelDialog();
                    if (response.getStatus()==Constants.RESPONSE_OK){
                        loadPaypalCheckOutActivity(data,countTextView.getText().toString(),"1");
                    }else {
                        loadPaypalCheckOutActivity(data,countTextView.getText().toString(),"0");
                    }
                }

                @Override
                public void onFailure() {
                    hideProgressBar();
                    loadPaypalCheckOutActivity(data,countTextView.getText().toString(),"0");
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void loadPaypalCheckOutActivity(PersonalCourseListModel data, String count, String s) {
        Intent intent=new Intent(getApplicationContext(),PaypalCheckoutActivity.class);
        intent.putExtra(Constants.EXTRAS_PERSONAL_COURSES_DETAIL,data);
        intent.putExtra(Constants.EXTRA_NUMBER_OF_TICKETS,count);
        intent.putExtra(Constants.EXTRAS_PAYPAL_PAYMENT_STATUS,s);
        startActivity(intent);
    }


    @OnClick(R.id.backImageView)
    public void onClick() {
        super.onBackPressed();
    }

    @OnClick({R.id.minusIcon, R.id.plusIcon, R.id.creditCardPaymentButton, R.id.paypalPaymentButton, R.id.oxxoPayment})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.minusIcon:
                if (Integer.valueOf(countTextView.getText().toString()) == 1) {
                    countTextView.setText("1");
                }else {
                    countTextView.setText(Integer.valueOf(countTextView.getText().toString()) - 1 + "");
                }
                break;
            case R.id.plusIcon:
                if (countTextView.getText().toString().equals(data.getMaxCapacity())){
                    countTextView.setText(data.getMaxCapacity());
                }else {
                    countTextView.setText(Integer.valueOf(countTextView.getText().toString()) + 1 + "");
                }
                break;
            case R.id.creditCardPaymentButton:
                Intent intent=new Intent(getApplicationContext(), ConecktaCheckoutActivity.class);
                intent.putExtra(Constants.EXTRAS_PERSONAL_COURSES_DETAIL,data);
                Log.w("Number of tickets",countTextView.getText().toString()+"--");
                intent.putExtra(Constants.EXTRA_NUMBER_OF_TICKETS,countTextView.getText().toString());
                startActivity(intent);
                break;
            case R.id.paypalPaymentButton:
                requestPaypalToken();
                break;
            case R.id.oxxoPayment:
                doOXXOPayment();
                break;
        }
    }

    private void doOXXOPayment() {
        if (InternetAvailability.isNetworkAvailable(getApplicationContext())){
            showProgressBar();
            PaymentRequestModel paymentRequestModel=new PaymentRequestModel();
            paymentRequestModel.setInPersonCourseId(data.getId());
            paymentRequestModel.setNumberOfPeople(countTextView.getText().toString());
            NetworkAdapter.getInstance(getApplication()).
                    payWithOXXO(paymentRequestModel, new ResponseCallback<GenericResponse<OXXOPaymentResponseModel>>(getApplicationContext()) {
                @Override
                public void onResponse(GenericResponse<OXXOPaymentResponseModel> response) {
                    hideProgressBar();
                    if (response.getStatus()==Constants.RESPONSE_OK){
                        loadOxxoPaymentConfimationActivity(response.getData(),data);
                    }
                }

                @Override
                public void onFailure() {
                    hideProgressBar();
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void loadOxxoPaymentConfimationActivity(OXXOPaymentResponseModel data, PersonalCourseListModel personalCourseListModel) {
        Intent intent=new Intent(getApplicationContext(), OxxoPaymentConfimationActivity.class);
        intent.putExtra(Constants.EXTRAS_OXXO_PAYMENT_DETAIL,data);
        intent.putExtra(Constants.EXTRAS_PERSONAL_COURSES_DETAIL,personalCourseListModel);
        intent.putExtra(Constants.EXTRA_NUMBER_OF_TICKETS,countTextView.getText().toString());
        startActivity(intent);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    public void showDialog(){
        progressDialog= new ProgressDialog(PersonalCourseDetailActivity.this);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void cancelDialog(){
        progressDialog.cancel();
    }
    @Override
    public void onCancel(int requestCode) {
        Log.w("Cancelled","YES");
        cancelDialog();
    }
}
