package com.medialima.zertu.ui.addcard;

public interface DialogListener {

    void onPositiveAction();

    void onNegativeAction();
}
