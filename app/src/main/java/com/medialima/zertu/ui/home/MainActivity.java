package com.medialima.zertu.ui.home;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.medialima.zertu.R;
import com.medialima.zertu.model.CurrentVersionResponseModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.home.contact.ContactFragment;
import com.medialima.zertu.ui.home.mypaymentorders.MyPaymentOrdersFragment;
import com.medialima.zertu.ui.home.myprofile.MyProfileFragment;
import com.medialima.zertu.ui.home.mytickets.MyTicketsFragment;
import com.medialima.zertu.ui.login.LoginActivity;
import com.medialima.zertu.ui.loginorregister.LoginOrRegisterActivity;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.medialima.zertu.utils.PreferenceUtil;
import com.medialima.zertu.vimeo.AccountPreferenceManager;
import com.vimeo.networking.VimeoClient;
import com.vimeo.networking.callbacks.AuthCallback;
import com.vimeo.networking.callbacks.ModelCallback;
import com.vimeo.networking.model.User;
import com.vimeo.networking.model.Video;
import com.vimeo.networking.model.VideoList;
import com.vimeo.networking.model.error.VimeoError;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.categoryTextView)
    TextView categoryTextView;
    @BindView(R.id.myProfileTextView)
    TextView myProfileTextView;
    @BindView(R.id.contactTextView)
    TextView contactTextView;
    @BindView(R.id.logoutTextView)
    TextView logoutTextView;
    private VimeoClient mApiClient = VimeoClient.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Initialize home fragment
        getSupportActionBar().setTitle(getString(R.string.category_small));
        moveToHomeFragment();

        getCurretVersion();

        if (mApiClient.getVimeoAccount().getAccessToken() == null) {
            // If there is no access token, fetch one on first app open
            authenticateWithClientCredentials();

        }else {
           // fetchParticularVideo("/me/videos");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
    }

    private void getCurretVersion() {
        if (InternetAvailability.isNetworkAvailable(MainActivity.this)){
            NetworkAdapter.getInstance(getApplicationContext()).
                    getCurrentVersion(new ResponseCallback<GenericResponse<CurrentVersionResponseModel>>(getApplicationContext()) {
                @Override
                public void onResponse(GenericResponse<CurrentVersionResponseModel> response) {
                    if (response.getStatus()== Constants.RESPONSE_OK){
                        try {
                            PackageInfo pInfo = getApplication().getPackageManager().getPackageInfo(getPackageName(), 0);
                            String version = pInfo.versionName;
                            Log.w("Device version",version);
                            Log.w("minVersion",response.getData().getMinAndroidVersion()+"");
                            if (versionCompare(version, response.getData().getMinAndroidVersion()) < 0) {
                                updateCurrentVersion();
                            }
//                            if (versionCompare(version, 2.0+"") < 0) {
//                                updateCurrentVersion();
//                            }
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure() {

                }
            });
        }else {
            Toast.makeText(getApplicationContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void updateCurrentVersion() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.new_version_available))
                .setPositiveButton(R.string.upgrade_now, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                    }
                });

        final android.app.AlertDialog dialog = builder.create();
        dialog.show();
//Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Boolean wantToCloseDialog = false;
                openPlayStore();
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    dialog.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
        });

        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    dialog.dismiss();
                }
                return true;
            }
        });
    }

    private void openPlayStore() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public static int versionCompare(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        // compare first non-equal ordinal number
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        return Integer.signum(vals1.length - vals2.length);
    }

    private void fetchParticularVideo(String uri) {

        VimeoClient.getInstance().fetchNetworkContent(uri, new ModelCallback<VideoList>(VideoList.class) {
            @Override
            public void success(VideoList videoList) {
                Log.w("Video Size",videoList.data.size()+"\n,");
                if (videoList != null && videoList.data != null && !videoList.data.isEmpty()) {
                    Video video = videoList.data.get(0); // just an example of getting the first video
                    String videoTitlesString = "";
                    for (int i=0;i<videoList.data.size();i++) {
                        videoTitlesString += videoList.data.get(i).name;
                        Log.w(i+"-->",videoTitlesString+"\n,");
                    }

                }
            }

            @Override
            public void failure(VimeoError error) {
                // voice the error
                Log.w("Video Failure",error.getErrorMessage());
            }
        });

//        VimeoClient.getInstance().fetchNetworkContent(uri1, new ModelCallback<Video>(Video.class) {
//            @Override
//            public void success(Video video) {
//                // use the video
//                Log.w("Video Success","YES");
//            }
//
//            @Override
//            public void failure(VimeoError error) {
//                // voice the error
//                Log.w("Video Failure",error.getDeveloperMessage());
//            }
//        });
    }

    private void authenticateWithClientCredentials() {
        mApiClient.authorizeWithClientCredentialsGrant(new AuthCallback() {
            @Override
            public void success() {
                Log.w("Authorization Success","YES");
//                String uri="video/229650671";
              //  fetchParticularVideo("/me/videos");


            }

            @Override
            public void failure(VimeoError error) {
                Log.w("Authorization Failure","YES");

            }
        });
    }
    private void moveToHomeFragment() {
        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, homeFragment);
        fragmentTransaction.commit();
    }

    public void switchFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, tag).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick({R.id.categoryTextView, R.id.myProfileTextView, R.id.contactTextView, R.id.logoutTextView, R.id.myTicketsTextView, R.id.myPaymentOrdersTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.categoryTextView:
                HomeFragment homeFragment = new HomeFragment();
                switchFragment(homeFragment,homeFragment.getClass().getName());
                changeTittle(getString(R.string.category_small));
                closeDrawer();
                break;
            case R.id.myProfileTextView:
                MyProfileFragment myProfileFragment = new MyProfileFragment();
                switchFragment(myProfileFragment,myProfileFragment.getClass().getName());
                changeTittle(getString(R.string.my_profile_small));
                closeDrawer();
                break;
            case R.id.contactTextView:
                ContactFragment contactFragment = new ContactFragment();
                switchFragment(contactFragment,contactFragment.getClass().getName());
                changeTittle(getString(R.string.contact_small));
                closeDrawer();
                break;

            case R.id.myTicketsTextView:
                MyTicketsFragment myTicketsFragment = new MyTicketsFragment();
                switchFragment(myTicketsFragment,myTicketsFragment.getClass().getName());
                changeTittle(getString(R.string.my_tickets_small));
                closeDrawer();
                break;
            case R.id.myPaymentOrdersTextView:
                MyPaymentOrdersFragment myPaymentOrdersFragment = new MyPaymentOrdersFragment();
                switchFragment(myPaymentOrdersFragment,myPaymentOrdersFragment.getClass().getName());
                changeTittle(getString(R.string.my_payment_orders_small));
                closeDrawer();
                break;
            case R.id.logoutTextView:
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(getString(R.string.logout))
                        .setMessage(getString(R.string.are_you_sure_want_to_logout))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                doLogout();
                            }
                        }).setNegativeButton(R.string.no, null).show();
                break;
        }
    }

    private void doLogout() {
        PreferenceUtil.clearPreference(getApplicationContext());
        LoginManager.getInstance().logOut();
        Intent intent = new Intent(MainActivity.this, LoginOrRegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    private void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public void changeTittle(String tittle){
        getSupportActionBar().setTitle(tittle);
    }


}
