package com.medialima.zertu.ui.paymentcheckout.oxxo;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.OXXOPaymentResponseModel;
import com.medialima.zertu.model.OxxoRequestModel;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.ui.home.coursedetail.CourseDetailFragment;
import com.medialima.zertu.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OxxoPaymentConfimationActivity extends AppCompatActivity {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;
    OXXOPaymentResponseModel data;
    PersonalCourseListModel personalCourseListModel;
    String noOfTicketsTextView;
    OxxoRequestModel oxxoRequestModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oxxo_payment_confimation);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        data=getIntent().getParcelableExtra(Constants.EXTRAS_OXXO_PAYMENT_DETAIL);
        personalCourseListModel=getIntent().getParcelableExtra(Constants.EXTRAS_PERSONAL_COURSES_DETAIL);
        noOfTicketsTextView=getIntent().getStringExtra(Constants.EXTRA_NUMBER_OF_TICKETS);
        oxxoRequestModel=getIntent().getParcelableExtra(Constants.OXXO_REQUEST);
        loadOxxoPaymentConfimationPage();
    }

    private void loadOxxoPaymentConfimationPage() {
        OxxoPaymentConfirmationFragment courseDetailFragment =OxxoPaymentConfirmationFragment.newInstance(data,personalCourseListModel,noOfTicketsTextView,oxxoRequestModel);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, courseDetailFragment);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.backImageView)
    public void onClick() {
        super.onBackPressed();
    }
}
