package com.medialima.zertu.ui.home.teacherdetail;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.CoursesModel;
import com.medialima.zertu.model.MastersModel;
import com.medialima.zertu.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TeacherDetailActivity extends AppCompatActivity {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;
    String title;
    MastersModel teacher;
    ArrayList<CoursesModel> courses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_detail);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        title=getIntent().getStringExtra(Constants.EXTRAS_NAME);
        teacher=getIntent().getParcelableExtra(Constants.EXTRAS_TEACHER);
        tittleTexView.setText(title);
        loadTeacherDetailFragment();

    }

    private void loadTeacherDetailFragment() {
        TeacherDetailFragment teacherDetailFragment =TeacherDetailFragment.newInstance(teacher);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, teacherDetailFragment);
        fragmentTransaction.commit();
    }


    @OnClick(R.id.backImageView)
    public void onClick() {
        super.onBackPressed();
    }
}
