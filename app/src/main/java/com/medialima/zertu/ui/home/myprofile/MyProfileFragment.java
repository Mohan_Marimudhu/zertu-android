package com.medialima.zertu.ui.home.myprofile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.common.RecyclerItemClickListener;
import com.medialima.zertu.model.CardModel;
import com.medialima.zertu.model.CreatePaymentRequestModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.ProfileResponseModel;
import com.medialima.zertu.model.UpdateProfileRequestModel;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.addcard.AddCreditCardActivity;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.medialima.zertu.utils.UserUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.updateProfileButton)
    RelativeLayout updateProfileButton;
    @BindView(R.id.caredListRecyclerView)
    RecyclerView caredListRecyclerView;
    @BindView(R.id.addCreditCardButton)
    RelativeLayout addCreditCardButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    public static int REQUEST_CODE = 100;
    ArrayList<CardModel> cardModelArrayList;
    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.newPasswordEditText)
    EditText newPasswordEditText;
    @BindView(R.id.confirmPasswordEditText)
    EditText confirmPasswordEditText;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MyProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);
        getProfile();
        getCardList();

        caredListRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), caredListRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
                        changeDefaultPayment(position);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
        return view;
    }

    private void getProfile() {
        if (InternetAvailability.isNetworkAvailable(getContext())){
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).
                    getProfile(new ResponseCallback<GenericResponse<ProfileResponseModel>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<ProfileResponseModel> response) {
                    hideProgressBar();
                    if (response.getStatus()==Constants.RESPONSE_OK){
                        nameEditText.setText(response.getData().getName());
                    }
                }

                @Override
                public void onFailure() {

                }
            });
        }else {
            Toast.makeText(getContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void changeDefaultPayment(int position) {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            CreatePaymentRequestModel createPaymentRequestModel = new CreatePaymentRequestModel();
            createPaymentRequestModel.setPaymentToken(cardModelArrayList.get(position).getId());
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).
                    changeDefaultPayment(createPaymentRequestModel, new ResponseCallback<GenericResponse>(getContext()) {
                        @Override
                        public void onResponse(GenericResponse response) {
                            if (response.getStatus() == 201) {
                                hideProgressBar();
                                getCardList();
                                Toast.makeText(getContext(), getString(R.string.updated), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure() {
                            hideProgressBar();
                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }


    private void getCardList() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).
                    getCardList(new ResponseCallback<GenericResponse<ArrayList<CardModel>>>(getContext()) {
                        @Override
                        public void onResponse(GenericResponse<ArrayList<CardModel>> response) {
                            hideProgressBar();
                            if (response.getStatus() == Constants.RESPONSE_OK && response.getData() != null && response.getData().size() != 0) {
                                cardModelArrayList = response.getData();
                                setUpRecyclverView(response.getData());
                            }

                        }

                        @Override
                        public void onFailure() {
                            hideProgressBar();
                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpRecyclverView(ArrayList<CardModel> data) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        caredListRecyclerView.setLayoutManager(layoutManager);
        caredListRecyclerView.setNestedScrollingEnabled(false);
        CardListAdapter cardListAdapter = new CardListAdapter(getContext(), data);
        caredListRecyclerView.setAdapter(cardListAdapter);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick({R.id.updateProfileButton, R.id.addCreditCardButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.updateProfileButton:
                if (nameEditText.getText().length()==0){
                    Toast.makeText(getContext(),getString(R.string.user_name_is_required),Toast.LENGTH_SHORT).show();
                }else if (newPasswordEditText.getText().length()==0){
                    Toast.makeText(getContext(),getString(R.string.password_is_required),Toast.LENGTH_SHORT).show();
                }else if (confirmPasswordEditText.getText().length()==0){
                    Toast.makeText(getContext(),getString(R.string.confirm_password_is_required),Toast.LENGTH_SHORT).show();
                }else if (!newPasswordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())){
                    Toast.makeText(getContext(),getString(R.string.password_and_confirm_password_did_not_match),Toast.LENGTH_SHORT).show();
                }else {
                    updateProfile();
                }
                break;
            case R.id.addCreditCardButton:
                Intent intent = new Intent(getContext(), AddCreditCardActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
                break;
        }
    }

    private void updateProfile() {
        if (InternetAvailability.isNetworkAvailable(getContext())){
            showProgressBar();
            UpdateProfileRequestModel updateProfileRequestModel=new UpdateProfileRequestModel();
            updateProfileRequestModel.setName(nameEditText.getText().toString());
            updateProfileRequestModel.setNewPassword(newPasswordEditText.getText().toString());
            NetworkAdapter.getInstance(getContext())
                    .updateProfile(updateProfileRequestModel, new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    if (response.getStatus()==Constants.RESPONSE_OK){
                        hideProgressBar();
                        Toast.makeText(getContext(),getString(R.string.updated),Toast.LENGTH_SHORT).show();
                        UserUtils.setUserPassword(getContext(), newPasswordEditText.getText().toString());
                        nameEditText.setEnabled(false);
                        clearPassowrdFields();
                    }
                }

                @Override
                public void onFailure() {

                }
            });
        }else {
            Toast.makeText(getContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void clearPassowrdFields() {
        newPasswordEditText.setText("");
        confirmPasswordEditText.setText("");
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getCardList();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
