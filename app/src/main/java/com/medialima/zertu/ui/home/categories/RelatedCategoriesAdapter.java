package com.medialima.zertu.ui.home.categories;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.medialima.zertu.R;
import com.medialima.zertu.model.AllCategoryResponseModel;
import com.medialima.zertu.ui.home.categories.categorydetail.CategoryDetailPage;
import com.medialima.zertu.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Mohan M on 5/8/2018.
 */

public class RelatedCategoriesAdapter extends RecyclerView.Adapter<RelatedCategoriesAdapter.ViewHolder> {
    Context context;
    ArrayList<AllCategoryResponseModel> data;
    public static class ViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView relatedCategoryImageView;
        TextView titleTextView, descriptionTextView;
        public ViewHolder(View v) {
            super(v);
            relatedCategoryImageView=(RoundedImageView)v.findViewById(R.id.relatedCategoryImageView);
            titleTextView=(TextView)v.findViewById(R.id.tittleTextView);
            descriptionTextView=(TextView)v.findViewById(R.id.descriptionTextView);
        }
    }

    public RelatedCategoriesAdapter(Context context,ArrayList<AllCategoryResponseModel> data) {
        this.context=context;
        this.data=data;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RelatedCategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.related_category_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RelatedCategoriesAdapter.ViewHolder holder, final int position) {
        if (data.get(position).getCoverImageUrl()==null){
            Picasso.with(context).load(R.drawable.cursos_presenciales).into(holder.relatedCategoryImageView);
        }else {
            try {
                Picasso.with(context).load(Constants.urlEncode(data.get(position).getCoverImageUrl())).fit().into(holder.relatedCategoryImageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        holder.titleTextView.setText(data.get(position).getName());
        holder.descriptionTextView.setText(Html.fromHtml(data.get(position).getDescription()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, CategoryDetailPage.class);
                intent.putExtra(Constants.EXTRAS_CATEGORY_ID,data.get(position).getId());
                intent.putExtra(Constants.EXTRAS_NAME,data.get(position).getName());
                context.startActivity(intent);
            }
        });
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }
}