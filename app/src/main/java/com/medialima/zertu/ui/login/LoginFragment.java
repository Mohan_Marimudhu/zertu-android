package com.medialima.zertu.ui.login;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.model.AuthenticationResponseModel;
import com.medialima.zertu.model.LoginRequestModel;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.ui.home.MainActivity;
import com.medialima.zertu.ui.passwordrecovery.PasswordRecoveryActivity;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.medialima.zertu.utils.UserUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.forgotPasswordTextView)
    TextView forgotPasswordTextView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void doLogin() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            LoginRequestModel loginRequestModel = new LoginRequestModel();
            loginRequestModel.setUsername(emailEditText.getText().toString());
            loginRequestModel.setPassword(passwordEditText.getText().toString());
            loginRequestModel.setClient_id(Constants.CLIENT_ID);
            loginRequestModel.setClient_secret(Constants.CLIENT_SECRETE);
            loginRequestModel.setGrant_type(Constants.GRAND_TYPE);
            loginRequestModel.setScope(Constants.SCOPE);
            NetworkAdapter.getInstance(getContext()).
                    login(loginRequestModel, new Callback<AuthenticationResponseModel>() {
                        @Override
                        public void onResponse(Call<AuthenticationResponseModel> call, Response<AuthenticationResponseModel> response) {
                            if (response != null && response.body() != null) {
                                UserUtils.setUserId(getContext(), emailEditText.getText().toString());
                                UserUtils.setUserPassword(getContext(), passwordEditText.getText().toString());
                                UserUtils.setAccessToken(getActivity(), response.body().getAccessToken());
                                UserUtils.setRefreshToken(getActivity(), response.body().getRefresh_token());
                                Log.w("Access Token", response.body().getAccessToken());
                                hideProgressBar();
                                moveToHomeActivity();
                            } else {
                                hideProgressBar();
                                Toast.makeText(getContext(), getString(R.string.username_or_password_is_incorrect), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<AuthenticationResponseModel> call, Throwable t) {
                            hideProgressBar();

                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void moveToHomeActivity() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @OnClick({R.id.loginButton, R.id.forgotPasswordTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                if (emailEditText.getText().length() == 0) {
                    Toast.makeText(getContext(), getString(R.string.email_is_required), Toast.LENGTH_SHORT).show();
                } else if (passwordEditText.getText().length() == 0) {
                    Toast.makeText(getContext(), getString(R.string.password_is_required), Toast.LENGTH_SHORT).show();
                } else {
                    doLogin();
                }
                break;
            case R.id.forgotPasswordTextView:
                loadPasswordRecoveryActivity();
                break;
        }
    }

    private void loadPasswordRecoveryActivity() {
        startActivity(new Intent(getContext(),PasswordRecoveryActivity.class));
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
