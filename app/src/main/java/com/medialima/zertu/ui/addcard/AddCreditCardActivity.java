package com.medialima.zertu.ui.addcard;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.ui.home.categories.personalCourseFragment.personalcourselist.PersonalCourseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddCreditCardActivity extends AppCompatActivity {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_credit_card);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        loadAddCreditCardPayment();
    }

    private void loadAddCreditCardPayment() {
        AddCreditCardFragment personalCourseFragment=new AddCreditCardFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, personalCourseFragment);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.backImageView)
    public void onClick() {
        super.onBackPressed();
    }
}
