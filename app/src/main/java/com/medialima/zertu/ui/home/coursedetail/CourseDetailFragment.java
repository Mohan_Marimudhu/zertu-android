package com.medialima.zertu.ui.home.coursedetail;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.medialima.zertu.R;
import com.medialima.zertu.model.CourseDetailResponseModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.vimeo.networking.VimeoClient;
import com.vimeo.networking.callbacks.ModelCallback;
import com.vimeo.networking.model.Video;
import com.vimeo.networking.model.VideoList;
import com.vimeo.networking.model.error.VimeoError;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CourseDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CourseDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tittleTextView)
    TextView tittleTextView;
    @BindView(R.id.descriptionTextView)
    WebView descriptionTextView;
    @BindView(R.id.availableSessionTextView)
    TextView availableSessionTextView;
    @BindView(R.id.availableSessionRecyclerView)
    RecyclerView availableSessionRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    CourseDetailResponseModel data;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String id;
    ArrayList<Video> videoArrayList = new ArrayList<>();
    private OnFragmentInteractionListener mListener;

    public CourseDetailFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static CourseDetailFragment newInstance(String id) {
        CourseDetailFragment fragment = new CourseDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_course_detail, container, false);
        ButterKnife.bind(this, view);
        getCourseDetail();
        return view;
    }

    private void fetchParticularVideo(String uri) {
        VimeoClient.getInstance().fetchNetworkContent(uri, new ModelCallback<VideoList>(VideoList.class) {
            @Override
            public void success(VideoList videoList) {
                if (videoList != null && videoList.data != null && !videoList.data.isEmpty()) {
                    if (data.getVideos().size() != 0) {
                        Log.w("Video Size", videoList.data.get(0) + "");
                        Log.w("Size-->", videoList.data.size() + "");
                        for (int i = 0; i < data.getVideos().size(); i++) {
//                            for (int j = 0; j < videoList.data.size(); j++) {
//                                Log.w("uri-->",videoList.data.get(j).uri+"");
//                                if (data.getVideos().get(i).getName().equals(videoList.data.get(j).name)) {
//                                    videoArrayList.add(videoList.data.get(j));
//                                }
//
//                            }
                            for (int j = 0; j < 5; j++) {
                                Log.w("uri-->", videoList.data.get(j).uri + "");
                                videoArrayList.add(videoList.data.get(j));

                            }
                        }
                        // setUpRecyclerView(videoArrayList);
                    }
                }
            }

            @Override
            public void failure(VimeoError error) {
                // voice the error
            }
        });
//        VimeoClient.getInstance().fetchNetworkContent(uri, new ModelCallback<VideoList>(VideoList.class) {
//            @Override
//            public void success(VideoList videoList) {
//                Log.w("Video Size",videoList.data.size()+"\n,");
//                if (videoList != null && videoList.data != null && !videoList.data.isEmpty()) {
//                    video=videoList;
//                    setUpRecyclerView(video);
//                }
//            }
//
//            @Override
//            public void failure(VimeoError error) {
//                // voice the error
//                Log.w("Video Failure",error.getErrorMessage());
//            }
//        });

//        VimeoClient.getInstance().fetchNetworkContent(uri1, new ModelCallback<Video>(Video.class) {
//            @Override
//            public void success(Video video) {
//                // use the video
//                Log.w("Video Success","YES");
//            }
//
//            @Override
//            public void failure(VimeoError error) {
//                // voice the error
//                Log.w("Video Failure",error.getDeveloperMessage());
//            }
//        });
    }

    private void getCourseDetail() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).getCourseById(id, new ResponseCallback<GenericResponse<CourseDetailResponseModel>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<CourseDetailResponseModel> response) {
                    hideProgressBar();
                    Log.w("Success", "YES");
                    if (response.getStatus() == Constants.RESPONSE_OK) {
                        setUpData(response.getData());
                        data = response.getData();
//                        fetchParticularVideo("/me/videos");
                        setUpRecyclerView(response.getData());
                    }
                }

                @Override
                public void onFailure() {
                    hideProgressBar();
                    Log.w("Success", "NO");
                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpRecyclerView(CourseDetailResponseModel data) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        availableSessionRecyclerView.setLayoutManager(layoutManager);
        availableSessionRecyclerView.setNestedScrollingEnabled(false);
        VideosAdapter videosAdapter = new VideosAdapter(getContext(), data);
        availableSessionRecyclerView.setAdapter(videosAdapter);

    }

    private void setUpData(CourseDetailResponseModel data) {
        if (data.getCategory().getCoverImageUrl() != null) {
            try {
                Picasso.with(getContext()).load(Constants.urlEncode(data.getCategory().getCoverImageUrl())).into(imageView);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        tittleTextView.setText(data.getName());
        descriptionTextView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        descriptionTextView.setLongClickable(false);
        descriptionTextView.setHapticFeedbackEnabled(false);
        descriptionTextView.loadDataWithBaseURL(null, data.getDescription(), "text/html", "utf-8", null);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
