package com.medialima.zertu.ui.home.coursedetail;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.medialima.zertu.R;
import com.medialima.zertu.model.CourseDetailResponseModel;
import com.medialima.zertu.model.VideosModel;
import com.medialima.zertu.ui.home.coursedetail.videodetail.VideoDetailActivity;
import com.medialima.zertu.utils.Constants;
import com.squareup.picasso.Picasso;
import com.vimeo.networking.model.Video;
import com.vimeo.networking.model.VideoFile;
import com.vimeo.networking.model.VideoList;
import com.vimeo.networking.model.playback.Play;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Mohan M on 5/21/2018.
 */

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.ViewHolder> {
    Context context;
    CourseDetailResponseModel data;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView videoNameTextView;
        ImageView imageView;
        ProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);
            videoNameTextView = (TextView) v.findViewById(R.id.videoNameTextView);
            imageView=(ImageView)v.findViewById(R.id.imageView);
            progressBar=(ProgressBar)v.findViewById(R.id.progressBar);
        }
    }

    public VideosAdapter(Context context, CourseDetailResponseModel data) {
        this.context = context;
        this.data = data;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public VideosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videos_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(VideosAdapter.ViewHolder holder, final int position) {
        holder.videoNameTextView.setText(data.getVideos().get(position).getName());
        getThumbnailUrl(data.getVideos().get(position).getUrl(),holder.imageView,holder.progressBar);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VideoDetailActivity.class);
                intent.putExtra(Constants.EXTRAS_VIDEO_URL, data.getVideos().get(position).getUrl());
                context.startActivity(intent);
            }
        });
    }

    private void getThumbnailUrl(String url, final ImageView imageView, final ProgressBar progressBar) {
        AsyncHttpClient asyncHttpClient=new AsyncHttpClient();
        Log.w("Url","https://vimeo.com/api/oembed.json?url="+url);
        asyncHttpClient.get("https://vimeo.com/api/oembed.json?url="+url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
               try {
                   Log.w("Response",new String(responseBody));
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    String thumbnailUrl=jsonObject.getString("thumbnail_url_with_play_button");
                    Picasso.with(context).load(thumbnailUrl).into(imageView);
                   progressBar.setVisibility(View.GONE);
                } catch (JSONException e) {
                    e.printStackTrace();
                   progressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    // "?player_id=player&autoplay=1&title=0&byline=0&portrait=0&api=1&maxheight=480&maxwidth=800"
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.getVideos().size();
    }
}