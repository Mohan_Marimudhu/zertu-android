package com.medialima.zertu.ui.passwordrecovery;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.medialima.zertu.R;
import com.medialima.zertu.ui.login.LoginFragment;

public class PasswordRecoveryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().hide();
        loadPasswordRecoveryFragment();
    }

    private void loadPasswordRecoveryFragment() {
        PasswordRecoveryFragment passwordRecoveryFragment = new PasswordRecoveryFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().replace(R.id.container, passwordRecoveryFragment);
        fragmentTransaction.commit();
    }

}
