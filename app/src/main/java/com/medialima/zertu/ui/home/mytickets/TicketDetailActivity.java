package com.medialima.zertu.ui.home.mytickets;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.UserTicketsModel;
import com.medialima.zertu.utils.Constants;

import net.glxn.qrgen.android.QRCode;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TicketDetailActivity extends AppCompatActivity {

    @BindView(R.id.backImageView)
    ImageView backImageView;
    @BindView(R.id.tittleTexView)
    TextView tittleTexView;
    UserTicketsModel userTicketsModel;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.paymentStatusTextView)
    TextView paymentStatusTextView;
    @BindView(R.id.courseNameTextView)
    TextView courseNameTextView;
    @BindView(R.id.numberOfTicketTextView)
    TextView numberOfTicketTextView;
    @BindView(R.id.totalTextView)
    TextView totalTextView;
    @BindView(R.id.startDateTextView)
    TextView startDateTextView;
    @BindView(R.id.endDateTextView)
    TextView endDateTextView;
    private static final int READ_CALENDAR_CODE = 100;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_CALENDAR_CODE) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                addToCalender();

            } else {
                ActivityCompat.requestPermissions(TicketDetailActivity.this,new String[]{Manifest.permission.READ_CALENDAR},
                        READ_CALENDAR_CODE);

            }

        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_detail);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userTicketsModel = getIntent().getParcelableExtra(Constants.EXTRAS_TICKET);
        setUpData();
    }

    private void setUpData() {
        Bitmap myBitmap = QRCode.from(userTicketsModel.getId()).
                withSize((int) getResources().getDimension(R.dimen.qr_code_sanner_size), (int) getResources().getDimension(R.dimen.qr_code_sanner_size)).bitmap();
        imageView.setImageBitmap(myBitmap);
        courseNameTextView.setText(" " + userTicketsModel.getInPersonCourse().getTitle());
        numberOfTicketTextView.setText(" " + userTicketsModel.getNumberOfTickets());
        totalTextView.setText(" $" + userTicketsModel.getAmountPaid());
        startDateTextView.setText("Fecha Inicio: " + userTicketsModel.getInPersonCourse().getStartDate());
        endDateTextView.setText("Fecha Fin: " + userTicketsModel.getInPersonCourse().getEndDate());
    }

    private void addToCalender() {
        String[] fullStartDate = userTicketsModel.getInPersonCourse().getStartDate().split(" ");
        String[] finalsStartDate;
        finalsStartDate = fullStartDate[0].split("-");
        String[] finalStartTime = fullStartDate[1].split(":");
        Calendar startingDate = Calendar.getInstance();
        startingDate.set(Calendar.YEAR, Integer.parseInt(finalsStartDate[0]));
        startingDate.set(Calendar.MONTH, Integer.parseInt(finalsStartDate[1]) - 1);
        startingDate.set(Calendar.DATE, Integer.parseInt(finalsStartDate[2]));
        startingDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(finalStartTime[0]));
        startingDate.set(Calendar.MINUTE, Integer.parseInt(finalStartTime[1]));


        String[] fullEndDate = userTicketsModel.getInPersonCourse().getEndDate().split(" ");
        String[] finalEndDate;
        finalEndDate = fullEndDate[0].split("-");
        String[] finalEndTime = fullEndDate[1].split(":");
        Calendar endingDate = Calendar.getInstance();
        endingDate.set(Calendar.YEAR, Integer.parseInt(finalEndDate[0]));
        endingDate.set(Calendar.MONTH, Integer.parseInt(finalEndDate[1]) - 1);
        endingDate.set(Calendar.DATE, Integer.parseInt(finalEndDate[2]));
        endingDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(finalEndTime[0]));
        endingDate.set(Calendar.MINUTE, Integer.parseInt(finalEndTime[1]));
        if (Build.VERSION.SDK_INT >= 14) {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startingDate.getTimeInMillis())
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endingDate.getTimeInMillis())
                    .putExtra(CalendarContract.Events.TITLE, userTicketsModel.getInPersonCourse().getTitle())
                    .putExtra(CalendarContract.Events.DESCRIPTION, userTicketsModel.getInPersonCourse().getDescription())
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
            startActivity(intent);
        }

        else {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra("beginTime", startingDate.getTimeInMillis());
            intent.putExtra("endTime", endingDate.getTimeInMillis());
            intent.putExtra("title", userTicketsModel.getInPersonCourse().getTitle());
            intent.putExtra("description",  userTicketsModel.getInPersonCourse().getDescription());
            intent.putExtra("allDay", false);
            intent.putExtra("calendar_id",  userTicketsModel.getInPersonCourse().getId());
            intent.putExtra("rule", "FREQ=YEARLY");
            startActivityForResult(intent, 100);
        }

    }
    @OnClick({R.id.backImageView, R.id.addToCalendarTextView})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backImageView:
                super.onBackPressed();
                break;
            case R.id.addToCalendarTextView:
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CALENDAR)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(TicketDetailActivity.this,new String[]{Manifest.permission.READ_CALENDAR},
                            READ_CALENDAR_CODE);
                }else {
                    addToCalender();
                }

                break;
        }
    }
}
