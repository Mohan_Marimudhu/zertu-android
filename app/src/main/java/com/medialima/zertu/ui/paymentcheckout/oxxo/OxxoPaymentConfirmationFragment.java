package com.medialima.zertu.ui.paymentcheckout.oxxo;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.OXXOPaymentResponseModel;
import com.medialima.zertu.model.OxxoRequestModel;
import com.medialima.zertu.model.PersonalCourseListModel;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OxxoPaymentConfirmationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OxxoPaymentConfirmationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    @BindView(R.id.bigTotalTextView)
    TextView bigTotalTextView;
    @BindView(R.id.referenceNumberTextView)
    TextView referenceNumberTextView;
    @BindView(R.id.courseNameTextView)
    TextView courseNameTextView;
    @BindView(R.id.priceTextView)
    TextView priceTextView;
    @BindView(R.id.numberOfTicketTextView)
    TextView numberOfTicketTextView;
    @BindView(R.id.totalTextView)
    TextView totalTextView;
    String noOfTickets;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    OXXOPaymentResponseModel data;
    PersonalCourseListModel personalCourseListModel;
    OxxoRequestModel oxxoRequestModel;
    private OnFragmentInteractionListener mListener;

    public OxxoPaymentConfirmationFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static OxxoPaymentConfirmationFragment newInstance(OXXOPaymentResponseModel data, PersonalCourseListModel personalCourseListModel,String noOfTickets, OxxoRequestModel oxxoRequestModel) {
        OxxoPaymentConfirmationFragment fragment = new OxxoPaymentConfirmationFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, data);
        args.putParcelable(ARG_PARAM2, personalCourseListModel);
        args.putString(ARG_PARAM3,noOfTickets);
        args.putParcelable(ARG_PARAM4,oxxoRequestModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = getArguments().getParcelable(ARG_PARAM1);
            personalCourseListModel = getArguments().getParcelable(ARG_PARAM2);
            noOfTickets=getArguments().getString(ARG_PARAM3);
            oxxoRequestModel=getArguments().getParcelable(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_oxxo_payment_confirmation, container, false);
        ButterKnife.bind(this, view);
        if (oxxoRequestModel==null){
            setUpPaymentConfirmationData();
        }else {
            setUpMyPaymentData();
        }

        return view;
    }

    private void setUpMyPaymentData() {
        bigTotalTextView.setText(" $"+oxxoRequestModel.getTotalAmount());
        referenceNumberTextView.setText(oxxoRequestModel.getReference());
        courseNameTextView.setText(" "+oxxoRequestModel.getTitle());
        numberOfTicketTextView.setText(" "+oxxoRequestModel.getNumberOfTickets());
        totalTextView.setText(" $"+oxxoRequestModel.getTotalAmount());
        try {
            double totalAmount=Double.valueOf(oxxoRequestModel.getTotalAmount());
            int pricePerPerson=(int)totalAmount/Integer.valueOf(oxxoRequestModel.getNumberOfTickets());
            priceTextView.setText(" $"+pricePerPerson);
        }catch (ArithmeticException e){
            e.printStackTrace();
        }
    }

    private void setUpPaymentConfirmationData() {
        double total=Double.valueOf(noOfTickets)*Double.valueOf(personalCourseListModel.getPrice());
        bigTotalTextView.setText(" $"+total);
        referenceNumberTextView.setText(data.getReference());
        courseNameTextView.setText(" "+personalCourseListModel.getTitle());
        priceTextView.setText(" $"+personalCourseListModel.getPrice());
        numberOfTicketTextView.setText(" "+noOfTickets);
        totalTextView.setText(" $"+total);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
