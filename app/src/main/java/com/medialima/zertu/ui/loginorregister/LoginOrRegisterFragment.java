package com.medialima.zertu.ui.loginorregister;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.medialima.zertu.R;
import com.medialima.zertu.model.AuthenticationResponseModel;
import com.medialima.zertu.model.FbLoginRequestModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.RegisterActivity.RegisterActivity;
import com.medialima.zertu.ui.home.MainActivity;
import com.medialima.zertu.ui.login.LoginActivity;
import com.medialima.zertu.utils.InternetAvailability;
import com.medialima.zertu.utils.UserUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginOrRegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginOrRegisterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.facebookLoginButton)
    Button facebookLoginButton;
    CallbackManager callbackManager;
    @BindView(R.id.registerButton)
    Button registerButton;
    @BindView(R.id.normalLoginButton)
    Button normalLoginButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginOrRegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginOrRegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginOrRegisterFragment newInstance(String param1, String param2) {
        LoginOrRegisterFragment fragment = new LoginOrRegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_or_register, container, false);
        ButterKnife.bind(this, view);
        initializeFacebook();
        return view;
    }

    private void initializeFacebook() {
        FacebookSdk.sdkInitialize(getContext());
        loginButton.setFragment(this);
        loginButton.setReadPermissions(Arrays.asList("email"));
        AppEventsLogger.activateApp(getContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        final String fbAccessToken = loginResult.getAccessToken().getToken();
                        Log.w("Facebook Access Token", fbAccessToken);
                        AccessToken token = loginResult.getAccessToken();
                        GraphRequest request = GraphRequest.newMeRequest(token,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {

                                        try {
                                            Log.w("Response", object.toString());
                                            String firstName = object.getString("first_name");
                                            String lastName = object.getString("last_name");
                                            String id = object.getString("id");
                                            String name = object.getString("name");
                                            String photo = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                            String emailId = object.getString("email");
                                            Log.w("Login Response", name + "\n" + firstName + "\n" + lastName + "\n" + id + "\n" + photo + "\n" + emailId);
                                            FbLoginRequestModel fbLoginRequestModel = new FbLoginRequestModel();
                                            fbLoginRequestModel.setFbToken(id);
                                            fbLoginRequestModel.setEmail(emailId);
                                            fbLoginRequestModel.setName(name);
                                            doFacebookLogin(fbLoginRequestModel);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name, last_name, picture, email, birthday, location, cover");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e("Error",exception.getLocalizedMessage());
                    }
                });

    }

    private void doFacebookLogin(FbLoginRequestModel fbLoginRequestModel) {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).fbLogin(fbLoginRequestModel,
                    new ResponseCallback<GenericResponse<AuthenticationResponseModel>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<AuthenticationResponseModel> response) {
                    if (response != null && response.getData() != null) {
                        UserUtils.setAccessToken(getActivity(), response.getData().getAccessToken());
                        UserUtils.setRefreshToken(getActivity(), response.getData().getRefresh_token());
                        hideProgressBar();
                        moveToHomeActivity();
                    } else {
                        hideProgressBar();
                        Toast.makeText(getContext(), getString(R.string.username_or_password_is_incorrect), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure() {

                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_LONG).show();
        }
    }

    private void moveToHomeActivity() {
        Intent intent=new Intent(getContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.facebookLoginButton)
    public void onClick() {
        loginButton.performClick();
    }

    @OnClick({R.id.registerButton, R.id.normalLoginButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.registerButton:
                moveToRegisterActivity();
                break;
            case R.id.normalLoginButton:
                moveToLoginActivity();
                break;
        }
    }

    private void moveToLoginActivity() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
    }

    private void moveToRegisterActivity() {
        Intent intent = new Intent(getContext(), RegisterActivity.class);
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }
}
