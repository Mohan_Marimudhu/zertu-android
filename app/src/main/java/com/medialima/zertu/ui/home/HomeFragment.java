package com.medialima.zertu.ui.home;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.model.AllCategoryResponseModel;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.home.categories.MyFragmentPageAdapter;
import com.medialima.zertu.ui.home.categories.RelatedCategoriesAdapter;
import com.medialima.zertu.ui.home.categories.pager.PagerFragment;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.vimeo.networking.VimeoClient;
import com.vimeo.networking.callbacks.ModelCallback;
import com.vimeo.networking.model.Video;
import com.vimeo.networking.model.error.VimeoError;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.viewPager)
    cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager viewPager;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    float downX;
    ArrayList<AllCategoryResponseModel> data;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        addViewPagerConstrains();
        getAllCategories();


        return view;
    }

    private void fetchMyVideo(String id) {

    }
    private void getAllCategories() {
        if (InternetAvailability.isNetworkAvailable(getContext())){
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).getAllCategory(
                    new ResponseCallback<GenericResponse<ArrayList<AllCategoryResponseModel>>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<ArrayList<AllCategoryResponseModel>> response) {
                    hideProgressBar();
                    if (response.getStatus()== Constants.RESPONSE_OK){
                        data=new ArrayList<AllCategoryResponseModel>();
                        if (isAdded()) {
                            AllCategoryResponseModel allCategoryResponseModel = new AllCategoryResponseModel();
                            allCategoryResponseModel.setName(getString(R.string.cursos_presenciales));
                            allCategoryResponseModel.setDescription(getString(R.string.cursos_presenciales_description));
                            data.add(allCategoryResponseModel);
                            data.addAll(response.getData());
                            setUpViewPager(data);
                            setUpCategroyRecyclerView(data);
                        }
                    }

                }

                @Override
                public void onFailure() {
                    hideProgressBar();
                }
            });
        }else {
            Toast.makeText(getContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpCategroyRecyclerView(ArrayList<AllCategoryResponseModel> data) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
       // recyclerView.setNestedScrollingEnabled(false);
        RelatedCategoriesAdapter relatedCategoriesAdapter = new RelatedCategoriesAdapter(getContext(),data);
        recyclerView.setAdapter(relatedCategoriesAdapter);
    }

    private void addViewPagerConstrains() {
        // Disable clip to padding
        viewPager.setClipToPadding(false);
        // set padding manually, the more you set the padding the more you see of prev & next page
        viewPager.setPadding(150, 0, 150, 0);
        // sets a margin b/w individual pages to ensure that there is a gap b/w them
        viewPager.setPageMargin(60);
    }

    private void setUpViewPager(final ArrayList<AllCategoryResponseModel> data) {
        List<Fragment> fragments = buildFragments(data);
        MyFragmentPageAdapter mPageAdapter = new MyFragmentPageAdapter(getContext(), getFragmentManager(), fragments,data);
        viewPager.setAdapter(mPageAdapter);
        viewPager.setInterval(4000);
        viewPager.startAutoScroll();
        viewPager.setCycle(true);
        viewPager.setBorderAnimation(false);
        viewPager.setStopScrollWhenTouch(true);
        viewPager.setCurrentItem(viewPager.getChildCount() * mPageAdapter.LOOPS_COUNT / 2, false);
       // viewPager.setCurrentItem(1,true);



    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private List<Fragment> buildFragments(ArrayList<AllCategoryResponseModel> data) {
        List<Fragment> fragments = new ArrayList<Fragment>();
        for (int i = 0; i < data.size(); i++) {
            Bundle b = new Bundle();
            b.putInt("position", i);
            fragments.add(PagerFragment.newInstance(data.get(i)));
        }

        return fragments;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
