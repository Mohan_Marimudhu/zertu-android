package com.medialima.zertu.ui.home.categories.categorydetail;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.squareup.picasso.Picasso;
import com.medialima.zertu.R;
import com.medialima.zertu.model.MastersModel;
import com.medialima.zertu.ui.home.teacherdetail.TeacherDetailActivity;
import com.medialima.zertu.utils.Constants;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Mohan M on 5/15/2018.
 */

public class AvailableTeacherAdapter extends RecyclerView.Adapter<AvailableTeacherAdapter.ViewHolder> {
    Context context;
    ArrayList<MastersModel> teachersModelList;
    public static class ViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView teachersImageView;
        TextView titleTextView, descriptionTextView;
        public ViewHolder(View v) {
            super(v);
            teachersImageView=(RoundedImageView)v.findViewById(R.id.teachersImageView);
            titleTextView=(TextView)v.findViewById(R.id.tittleTextView);
            descriptionTextView=(TextView)v.findViewById(R.id.descriptionTextView);
        }
    }

    public AvailableTeacherAdapter(Context context,ArrayList<MastersModel> teachersModelList) {
        this.context=context;
        this.teachersModelList=teachersModelList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AvailableTeacherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.available_teachers_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        AvailableTeacherAdapter.ViewHolder vh = new AvailableTeacherAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(AvailableTeacherAdapter.ViewHolder holder, final int position) {
        try {
            Picasso.with(context).load(Constants.urlEncode(teachersModelList.get(position).getPrimaryPicUrl())).into(holder.teachersImageView);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        holder.titleTextView.setText(teachersModelList.get(position).getName());
        holder.descriptionTextView.setText(Html.fromHtml(teachersModelList.get(position).getDescription()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, TeacherDetailActivity.class);
                intent.putExtra(Constants.EXTRAS_NAME,teachersModelList.get(position).getName());
                intent.putExtra(Constants.EXTRAS_TEACHER,teachersModelList.get(position));
                context.startActivity(intent);
            }
        });
    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return teachersModelList.size();
    }
}