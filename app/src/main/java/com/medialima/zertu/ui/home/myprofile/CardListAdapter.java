package com.medialima.zertu.ui.home.myprofile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.medialima.zertu.R;
import com.medialima.zertu.model.CardModel;

import java.util.ArrayList;

/**
 * Created by Mohan M on 6/14/2018.
 */

public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.ViewHolder> {
    ArrayList<CardModel> cardModelArrayList;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView cardTypeTextView,maskedNumberTextView;
        ImageView imageView;
        public ViewHolder(View v) {
            super(v);
            cardTypeTextView=(TextView)v.findViewById(R.id.cardTypeTextView);
            maskedNumberTextView=(TextView)v.findViewById(R.id.maskedNumberTextView);
            imageView=(ImageView)v.findViewById(R.id.imageView);
        }
    }

    public CardListAdapter(Context context,ArrayList<CardModel> cardModelArrayList) {
        this.context=context;
        this.cardModelArrayList=cardModelArrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_list_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardListAdapter.ViewHolder holder, int position) {

        holder.cardTypeTextView.setText(cardModelArrayList.get(position).getType());
        holder.maskedNumberTextView.setText(cardModelArrayList.get(position).getMaskedNumber());
        if (cardModelArrayList.get(position).isDefaultMethod()){
            holder.imageView.setVisibility(View.VISIBLE);
        }else {
            holder.imageView.setVisibility(View.GONE);
        }

    }



    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return cardModelArrayList.size();
    }
}
