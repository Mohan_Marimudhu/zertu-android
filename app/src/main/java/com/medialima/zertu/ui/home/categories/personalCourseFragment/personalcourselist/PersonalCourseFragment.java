package com.medialima.zertu.ui.home.categories.personalCourseFragment.personalcourselist;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.PersonalCourseListModel;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PersonalCourseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalCourseFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.tittleTextView)
    TextView tittleTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.courseDescriptionTextView)
    TextView courseDescriptionTextView;
    @BindView(R.id.coursesRecyclerView)
    RecyclerView coursesRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.emptyCourseTextView)
    TextView emptyCourseTextView;
    @BindView(R.id.bottomLayout)
    LinearLayout bottomLayout;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PersonalCourseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PersonalCourseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonalCourseFragment newInstance(String param1, String param2) {
        PersonalCourseFragment fragment = new PersonalCourseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_course, container, false);
        ButterKnife.bind(this, view);
        setData();
        getPersonalCourseList();

        return view;
    }

    private void getPersonalCourseList() {
        if (InternetAvailability.isNetworkAvailable(getContext())) {
            showProgressBar();
            NetworkAdapter.getInstance(getContext()).
                    getPersonalCourseList(new ResponseCallback<GenericResponse<ArrayList<PersonalCourseListModel>>>(getContext()) {
                        @Override
                        public void onResponse(GenericResponse<ArrayList<PersonalCourseListModel>> response) {
                            hideProgressBar();
                            if (response.getStatus() == Constants.RESPONSE_OK) {
                                if (response.getData() == null || response.getData().size() == 0) {
                                    emptyCourseTextView.setVisibility(View.VISIBLE);
                                    bottomLayout.setVisibility(View.GONE);
                                }else {
                                    setUpRecyclerView(response.getData());
                                }
                            }
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
        } else {
            Toast.makeText(getContext(), getString(R.string.internet_is_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpRecyclerView(ArrayList<PersonalCourseListModel> data) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        coursesRecyclerView.setLayoutManager(layoutManager);
        coursesRecyclerView.setNestedScrollingEnabled(false);
        PersonalCourseListAdapter personalCourseListAdapter = new PersonalCourseListAdapter(getContext(), data);
        coursesRecyclerView.setAdapter(personalCourseListAdapter);
    }

    private void setData() {
        Picasso.with(getContext()).load(R.drawable.cursos_presenciales).into(imageView);
        tittleTextView.setText(getString(R.string.cursos_presenciales));
        descriptionTextView.setText(getString(R.string.cursos_presenciales_description));
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        progressBar.setVisibility(View.GONE);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
