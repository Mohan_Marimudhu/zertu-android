package com.medialima.zertu.ui.RegisterActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.medialima.zertu.R;
import com.medialima.zertu.model.GenericResponse;
import com.medialima.zertu.model.RegisterRequestModel;
import com.medialima.zertu.network.NetworkAdapter;
import com.medialima.zertu.network.ResponseCallback;
import com.medialima.zertu.ui.login.LoginActivity;
import com.medialima.zertu.utils.Constants;
import com.medialima.zertu.utils.InternetAvailability;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @BindView(R.id.userNameEditText)
    EditText userNameEditText;
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;
    @BindView(R.id.confirmPasswordEditText)
    EditText confirmPasswordEditText;
    @BindView(R.id.registerButton)
    Button registerButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.registerButton)
    public void onClick() {
        if (userNameEditText.getText().length()==0){
            Toast.makeText(getContext(),getString(R.string.user_name_is_required),Toast.LENGTH_SHORT).show();
        }else if (emailEditText.getText().length()==0){
            Toast.makeText(getContext(),getString(R.string.email_is_required),Toast.LENGTH_SHORT).show();
        }else if (!isValidEmail(emailEditText.getText().toString())){
            Toast.makeText(getContext(),getString(R.string.please_enter_valid_email_address),Toast.LENGTH_SHORT).show();
        }else if (passwordEditText.getText().length()==0){
            Toast.makeText(getContext(),getString(R.string.password_is_required),Toast.LENGTH_SHORT).show();
        }else if (confirmPasswordEditText.getText().length()==0){
            Toast.makeText(getContext(),getString(R.string.confirm_password_is_required),Toast.LENGTH_SHORT).show();
        }else if (!passwordEditText.getText().toString().equals(confirmPasswordEditText.getText().toString())){
            Toast.makeText(getContext(),getString(R.string.password_and_confirm_password_did_not_match),Toast.LENGTH_SHORT).show();
        }else {
            doRegister();
        }

    }

    private void doRegister() {
        if (InternetAvailability.isNetworkAvailable(getContext())){
            showProgressBar();
            RegisterRequestModel registerRequestModel=new RegisterRequestModel();
            registerRequestModel.setUsername(emailEditText.getText().toString());
            registerRequestModel.setEmail(emailEditText.getText().toString());
            registerRequestModel.setPassword(passwordEditText.getText().toString());
            registerRequestModel.setName(userNameEditText.getText().toString());
            registerRequestModel.setAddress_line_1("");
            registerRequestModel.setAddress_line_2("");
            NetworkAdapter.getInstance(getContext()).register(registerRequestModel,
                    new ResponseCallback<GenericResponse<String>>(getContext()) {
                @Override
                public void onResponse(GenericResponse<String> response) {
                    hideProgressBar();
                    if (response.getStatus()== Constants.RESPONSE_OK){
                        getActivity().finish();
                        moveToLoginActivity();
                    }else {
                        Toast.makeText(getContext(),getString(R.string.user_already_registered),Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure() {
                    hideProgressBar();
                    Toast.makeText(getContext(),getString(R.string.user_already_registered),Toast.LENGTH_SHORT).show();

                }
            });

        }else {
            Toast.makeText(getContext(),getString(R.string.internet_is_not_available),Toast.LENGTH_SHORT).show();
        }
    }

    private void moveToLoginActivity() {
        Intent intent=new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    private void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
