package com.medialima.zertu.utils;

import android.content.Context;
import android.text.TextUtils;


public class UserUtils {

    public static void setAccessToken(Context context, String accessToken) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_ACCESS_TOKEN, accessToken);
        }
    }

    public static String getAccessToken(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_ACCESS_TOKEN);
        }
        return null;
    }

    public static void saveUserId(Context context, String userId) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_USER_ID, userId);
        }
    }

    public static void setEmailId(Context context, String userName) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_EMAIL_ID, userName);
        }
    }

    public static String getEmailId(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_EMAIL_ID);
        }
        return null;
    }

    public static void setUserPassword(Context context, String password) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_USER_PASSWORD, password);
        }
    }

    public static String getUserPassword(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_USER_PASSWORD);
        }
        return null;
    }

    public static void setRefreshToken(Context context, String refresh_token) {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_REFRESH_TOKEN, refresh_token);
        }
    }

    public static String getRefreshToken(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_REFRESH_TOKEN);
        }
        return null;
    }

    public static boolean isLoggedIn(Context context) {
        if (context != null) {
            return !TextUtils.isEmpty(UserUtils.getAccessToken(context));
        }
        return false;
    }


    public static String getUserId(Context context) {
        if (context != null) {
            return PreferenceUtil.getStringFromPref(context, Constants.SP_USER_ID);
        }
        return null;
    }

    public static void setUserId(Context context, String id)
    {
        if (context != null) {
            PreferenceUtil.addStringToPref(context, Constants.SP_USER_ID, id);
        }

    }

//    public static void setUserProfile(Context context, User user) {
//        if (context != null) {
//            PreferenceUtil.addStringToPref(context, Constants.SP_USER_ID, user.getId());
//            PreferenceUtil.addStringToPref(context, Constants.SP_USER_PROFILE, new Gson().toJson(user));
//        }
//    }
//
//    public static User getUserProfile(Context context) {
//        if (context != null) {
//            return new Gson().fromJson(PreferenceUtil.getStringFromPref(context, Constants.SP_USER_PROFILE), User.class);
//        }
//        return null;
//    }
}
