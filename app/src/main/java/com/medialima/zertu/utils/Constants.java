package com.medialima.zertu.utils;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Constants {
    public static String IMAGE_BASE_URL="https://s3-us-west-1.amazonaws.com/zertu/";

//    NETWORK RESPONSE
    public static int RESPONSE_OK=200;

//    API SECURITY
    public static String GRAND_TYPE="password";
    public static String SCOPE="read write";
    public static String CLIENT_SECRETE="zertu";
    public static String CLIENT_ID="zertu";
    public static String REFRESH_TOKEN="refresh_token";

//    EXTRAS
    public static String EXTRAS_CATEGORY_ID="EXTRA_CATEGORY_ID";
    public static String EXTRAS_NAME="EXTRA_NAME";
    public static String EXTRAS_TEACHER="EXTRA_TEACHERS";
    public static String EXTRAS_COURSE_ID="EXTRA_COURSE_ID";
    public static String EXTRAS_VIDEO_URL="EXTRA_VIDEO_URL";
    public static String EXTRAS_PERSONAL_COURSES_DETAIL="EXTRAS_PERSONAL_COURSES_DETAIL";
    public static String EXTRA_NUMBER_OF_TICKETS ="EXTRA_NUMBER_OF_TICKETS";
    public static String EXTRAS_OXXO_PAYMENT_DETAIL="EXTRAS_OXXO_PAYMENT_DETAIL";
    public static String EXTRAS_PAYPAL_PAYMENT_STATUS="EXTRAS_PAYPAL_PAYMENT_STATUS";
    public static String EXTRAS_TICKET="EXTRAS_TICKETS";
    public static String OXXO_REQUEST="OXXO_REQUEST";

//    SHARED PREFERENCE KEYS
    public static final String SP_USER_ID = "sp_user_id";
    public static final String SP_USER_PROFILE = "sp_user_profile";
    public static final String SP_ACCESS_TOKEN = "sp_access_token";
    public static final String SP_EMAIL_ID = "sp_email.com";
    public static final String SP_USER_PASSWORD = "sp_user_password";
    public static final String SP_REFRESH_TOKEN = "sp_refresh_token";

    public static String urlEncode(String unEncodedString) throws UnsupportedEncodingException {
        String url = URLEncoder.encode(unEncodedString, "utf-8");
         return  Constants.IMAGE_BASE_URL+url;
    }


}

